﻿using UnityEngine;
using System.Collections;

public class PlayerTest : MonoBehaviour {

	const float ControlForce = 2;
	const float GravityForce = 10;

	float mass = 1;
	Vector3 force = Vector3.zero;
	Vector3 velocity = Vector3.zero;
	float maxSpeed = 4;


	void Start () {
		Application.targetFrameRate = 40;
	}

	// Update is called once per frame
	void Update () {
		Integration();
		force = transform.up;
	}



	void Integration () {
		Vector3 a = force / mass;
		velocity += a * Time.deltaTime;

		velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

//				Debug.Log("Velocity = " + velocity + ", " + (velocity*Time.deltaTime).magnitude);


		Debug.Log("DT = " + Time.deltaTime);


//		transform.Translate(0, (velocity * Time.deltaTime).magnitude, 0);

		transform.Translate(velocity * Time.deltaTime);

		//		transform.position = Vector3.Lerp(transform.position, transform.position + velocity * Time.deltaTime, Time.deltaTime);
	}


//	bool mouseDown() {
//		if(Input.GetMouseButtonDown(0))
//			return true;
//		return false;
//	}

}
