﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	public Player player;


	void Update () {
		CameraFollowPlayer();

	}


	void CameraFollowPlayer() {
		transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z -60);
	}


}
