﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MyJoyStick : MonoBehaviour {
	public GameObject smallJoy;
	public Player player;
	public bool canMove { get; private set;}
	public bool down { get; private set;}


	Touch touch;
	Vector2 mousePos;
	Vector2 JoyPos;
	Vector2 movement;

	bool checkTouchLeaveJoystick;
	int idTouch;
	float R;
	float angle;
	float timePointerDown;


	void Start () {
		R = transform.GetComponent<Collider2D>().bounds.size.x/2;
	}


	public void MouseDown()	{
		timePointerDown = Time.time;
		#if UNITY_EDITOR
		Vector2 vec = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
		mousePos = vec;
		smallJoy.transform.position = mousePos;
		movement = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);


		#else
		for(int i=0; i< Input.touchCount; i++) {
			if(isTouchJoystick(Input.GetTouch(i)))
				touch = Input.GetTouch(i);
		}
		
		mousePos = Camera.main.ScreenToWorldPoint(new Vector2(touch.position.x, touch.position.y));
		smallJoy.transform.position = mousePos;
		movement = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
		#endif
		down = true;

	}


	public void MouseUp() {
		smallJoy.transform.position = transform.position;
		down = false;
		if (Time.time - timePointerDown > 1f) {
			player.BeginSlow();
		}

	}



	public void endDrags() {
		movement = Vector2.zero;
		smallJoy.transform.position = transform.position;
		player.BeginSlow();
	}


	public void BeginDrag()	{
		canMove = true;
		player.BeginFly();
		for(int i = 0; i < Input.touchCount; i++) {
			if(isTouchJoystick(Input.GetTouch(i)))
				idTouch = Input.GetTouch(i).fingerId;
		}
			
	}
		


	bool isTouchJoystick(Touch t) {
		Vector2 touchVec = Camera.main.ScreenToWorldPoint(new Vector2(t.position.x, t.position.y));
		if(Vector2.Distance(touchVec, JoyPos) <= R)
			return true;
		return false;
	}



	void Update() {
		JoyPos = new Vector2(transform.position.x, transform.position.y);
		if(down && Time.time - timePointerDown > 1f && player.state != Player.PlayerState.FallDown 
			&& player.state != Player.PlayerState.Break) {
			canMove = true;
			player.BeginFly();
			#if UNITY_EDITOR
			Vector2 vec = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
			mousePos = vec;
			movement = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);


			#else
			for(int i=0; i< Input.touchCount; i++) {
			if(isTouchJoystick(Input.GetTouch(i)))
			touch = Input.GetTouch(i);
			}

			mousePos = Camera.main.ScreenToWorldPoint(new Vector2(touch.position.x, touch.position.y));
			movement = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
			#endif

		}

		if(!gameObject.activeInHierarchy) {
			movement = Vector2.zero;
		}

		if(player.state == Player.PlayerState.Break) {
			smallJoy.transform.position = transform.position;
			down = false;
			movement = Vector2.zero;
		}

	}


	public void MouseDrag()	{
		
		#if UNITY_EDITOR
			mousePos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
		#else

		if(!checkTouchLeaveJoystick) {
			for(int i=0; i<Input.touchCount; i++) {
				if(Input.GetTouch(i).fingerId == idTouch) {
					touch = Input.GetTouch(i);
					Vector2 touchWorld = Camera.main.ScreenToWorldPoint(new Vector2(touch.position.x, touch.position.y));
					mousePos = touchWorld;
				}

			}

		}
		#endif


		if( Vector2.Distance(mousePos, JoyPos) <= R)
			smallJoy.transform.position = mousePos;
		else {
			Vector2 vecA = transform.up;
			Vector2 vecB = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
			float cosAlpha = (vecA.x * vecB.x + vecA.y* vecB.y) / (vecB.magnitude);
			float Alpha = Mathf.Acos(cosAlpha) * Mathf.Rad2Deg;
			if(vecB.x >=0)
				Alpha = 360-Alpha;
			angle = Alpha;
			float X = R* Mathf.Sin(Alpha * Mathf.Deg2Rad);
			float Y = R * Mathf.Cos(Alpha * Mathf.Deg2Rad);
			Vector2 smallPos = new Vector2(-X + transform.position.x, Y + transform.position.y);
			smallJoy.transform.position = smallPos;

		}

		movement = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
	}


	public Vector2 GetMovement() {
		return movement;
	}


	public void SetMovement(Vector2 vec) {
		movement = vec;
	}



	public void SetCanMove(bool bl) {
		canMove = bl;
	}
}
