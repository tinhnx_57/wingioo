﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	
	public enum PlayerState {
		Idle, 
		Fly, 
		FallDown, 
		Break, 
		MovingSlow
	};

	public enum WeaponType {
		Normal, 
		Boom, 
		ThreeBullet, 
		Rocket, 
		Punch, 
		Lightning
	};

	public GameObject head;
	public GameObject fallDownParticle;
	public GameObject circleAround;
	public GameObject tail;
	public GameObject trailRender;
	public GameObject syncComponent;
	public GameObject parentHealthBar;
	public GameObject childHealthBar;
	public GameObject borderHealth;
	public GameObject radaOfPlayer;
	public GameObject upSea;

	public Material tailMaterial;
	public Image weaponImageInfo;
	public KillSomeone killSomeone;
	public MyJoyStick baseJoyStick;

	public Text txtNotification;
	public Text txtWeaponLeft;

	public PlayerState state {get; private set;}
	public WeaponType weapon {get; private set;}
	public float speedMoving;
	public float speedPunch;
	public float rotateSpeed;

	public bool isDie {get; private set;}
	public string namePlayer {get; private set;}
	public int health {get; private set;}
	public int score {get; private set;}
	public float timeLoadNewGame {get; private set;}
	float timeStartSpeedUp;

	Sprite[] playerSprite;

	public bool havePunch {get; private set;}
	bool canRotate;
	bool isUnvunerable;
	int totalBullet;
	float speedWhenStart;
	float timeOutOfRightLeft;
	float heightborder;
	float widthborder;
	float angle;
	float speedSlowMoving;
	float borderHealthLengthWhenStart;
	float timePlayGame;
	const float HEALTHSTART = 100;

	Quaternion desRotation = Quaternion.identity;
	float rotationSpeed = 5;

	float mass = 1;
	float force = 1;
	float velocityUpBorder = 0;

	void InitGame() {
		health = 100; 
		score = 0;
		state = PlayerState.Idle;
		weapon = WeaponType.Normal;
		canRotate = false;
		havePunch = false;
		isUnvunerable = true;
		create3PartOneTime = false;
		timePlayGame = Time.time;
		playerSprite = new Sprite[14];
		totalBullet = -10;
		namePlayer = "TinhNX";
		txtNotification.gameObject.SetActive(false);
		speedWhenStart = speedMoving;
		speedSlowMoving = speedMoving;
		head.SetActive(false);
		trailRender.SetActive(false);
		killSomeone.gameObject.SetActive(false);
		fallDownParticle.SetActive(true);
		radaOfPlayer.SetActive(false);
		ChangeTailColor(new Color(1,1,1,1));

		heightborder = Background.Instant.getHeightBorder();
		widthborder = Background.Instant.getWidthBorder();

		parentHealthBar.transform.position = new Vector3(transform.position.x, transform.position.y -0.7f, transform.position.z);
		borderHealthLengthWhenStart = borderHealth.GetComponent<SpriteRenderer>().bounds.size.x;

		Sprite spr = Resources.Load<Sprite>("Image/PowerUp/1");
		weaponImageInfo.sprite = spr;
		txtWeaponLeft.text = "∞";

		CreateSpriteArray();
	}


	void Start () {
		InitGame();
		StartCoroutine(EscapeUnvunerable());
	}
		


	void Update () {
		CheckPlayerState();
		Rotate ();
		Translate();
		LoadSpriteWhenRotate();
		ReturnNormalBullet();
		controlOutOfScreen();
		AddHealthWhenIdle();
		ScaleBorderHealthBarWhenBeTheKingOrNot();
		TranslateChildHealthBar();
		CreateTouchSeaWhenUpSurface();
	}

	public Vector3 Velocity{
		get {return transform.up;}
	}

	void LoadSpriteWhenRotate () {
		float eulerAnglesZ = transform.eulerAngles.z;
		Sprite spr = new Sprite();
		if (eulerAnglesZ >= 0 && eulerAnglesZ <= 15 ) {
			spr = playerSprite[0]; 
		}else if (eulerAnglesZ > 15 && eulerAnglesZ <= 30) {
			spr = playerSprite[1];
		}else if (eulerAnglesZ > 30 && eulerAnglesZ <= 45) {
			spr = playerSprite[2];
		}else if (eulerAnglesZ > 45 && eulerAnglesZ <= 55) {
			spr = playerSprite[3];
		}else if (eulerAnglesZ > 55 && eulerAnglesZ <= 70) {
			spr = playerSprite[4];
		}else if (eulerAnglesZ > 70 && eulerAnglesZ <= 80) {
			spr = playerSprite[5];
		}else if (eulerAnglesZ > 80 && eulerAnglesZ <= 90) {
			spr = playerSprite[6];
		}

		else if (eulerAnglesZ >= 90 && eulerAnglesZ <= 105 ) {
			spr = playerSprite[6];
		}else if (eulerAnglesZ > 105 && eulerAnglesZ <= 120) {
			spr = playerSprite[5];
		}else if (eulerAnglesZ > 120 && eulerAnglesZ <= 135) {
			spr = playerSprite[4];
		}else if (eulerAnglesZ > 135 && eulerAnglesZ <= 150) {
			spr = playerSprite[3];
		}else if (eulerAnglesZ > 150 && eulerAnglesZ <= 160) {
			spr = playerSprite[2];
		}else if (eulerAnglesZ > 160 && eulerAnglesZ <= 170) {
			spr = playerSprite[1];
		}else if (eulerAnglesZ > 170 && eulerAnglesZ <= 180) {
			spr = playerSprite[0];
		}

		else if (eulerAnglesZ > 180 && eulerAnglesZ <= 195 ) {
			spr = playerSprite[7];
		}else if (eulerAnglesZ > 195 && eulerAnglesZ <= 210) {
			spr = playerSprite[8];
		}else if (eulerAnglesZ > 210 && eulerAnglesZ <= 225) {
			spr = playerSprite[9];
		}else if (eulerAnglesZ > 225 && eulerAnglesZ <= 240) {
			spr = playerSprite[10];
		}else if (eulerAnglesZ > 240 && eulerAnglesZ <= 250) {
			spr = playerSprite[11];
		}else if (eulerAnglesZ > 250 && eulerAnglesZ <= 260) {
			spr = playerSprite[12];
		}else if (eulerAnglesZ > 260 && eulerAnglesZ <= 270) {
			spr = playerSprite[13];
		}

		else if (eulerAnglesZ > 270 && eulerAnglesZ <= 285 ) {
			spr = playerSprite[13];
		}else if (eulerAnglesZ > 285 && eulerAnglesZ <= 300) {
			spr = playerSprite[12];
		}else if (eulerAnglesZ > 300 && eulerAnglesZ <= 315) {
			spr = playerSprite[11];
		}else if (eulerAnglesZ > 315 && eulerAnglesZ <= 330) {
			spr = playerSprite[10];
		}else if (eulerAnglesZ > 330 && eulerAnglesZ <= 340) {
			spr = playerSprite[9];
		}else if (eulerAnglesZ > 340 && eulerAnglesZ <= 350) {
			spr = playerSprite[8];
		}else if (eulerAnglesZ > 350 && eulerAnglesZ <= 360) {
			spr = playerSprite[7];
		}
			
		gameObject.GetComponent<SpriteRenderer>().sprite = spr;
	}



	void CheckPlayerState() {
		if ((!IsTheKing() && health <= 10 && health > 0) || (IsTheKing() && health <= 20 && health > 0)) {
			state = PlayerState.FallDown;
			transform.GetComponent<Rigidbody2D> ().gravityScale = 1f;
			fallDownParticle.SetActive(true);

			if (weapon == WeaponType.Punch) {
				ChangeWeapon(WeaponType.Normal);
				Color normalColor = new Color(255, 255, 255);
				ChangeTailColor(normalColor);

				Sprite spr = Resources.Load<Sprite>("Image/PowerUp/1");
				weaponImageInfo.sprite = spr;
				txtWeaponLeft.text = "∞";
				havePunch = false;
			}
		}else if (health <= 0) {
			state = PlayerState.Break;
		}else {
			fallDownParticle.SetActive(false);
			transform.GetComponent<Rigidbody2D> ().gravityScale = 0;
			if(state == Player.PlayerState.FallDown) {
				Idle();
			}
		}

		if(Time.time - timeOutOfRightLeft >= 2.5f && timeOutOfRightLeft != 0)
			state = PlayerState.Break;

		if(state == PlayerState.Break) {
			isDie = true;
			canRotate = false;
			head.SetActive(false);
			circleAround.SetActive(false);
			parentHealthBar.SetActive(false);
			txtNotification.gameObject.SetActive(false);
			tail.SetActive(false);
			timeOutOfRightLeft = 0;
			if(score > PlayerPrefs.GetInt("BEST"))
				PlayerPrefs.SetInt("BEST", score);
			PlayerPrefs.SetInt("YOURSCORE",score);
			AudioControl.Instant.PlayerDie();
			DisableLockEnemyComponent();
			DisableRada();
			Create3PartAfterBreak();
			CreateBreakComponent();
			CreateComponentAfterDie();
			MainGame.Instant.LoadCanvasWhenDie();
			gameObject.SetActive(false);

		}
	}
		

	IEnumerator EscapeUnvunerable() {
		yield return new WaitForSeconds(3);
		isUnvunerable = false;
		trailRender.SetActive(true);
	}
		

	float timeAddHealth;
	void AddHealthWhenIdle () {
		if (state == PlayerState.Idle && Time.time - timeAddHealth > 0.5f) {
			if (!IsTheKing() && health + 2 <= 100) {
				health += 2;
			}
			else if (IsTheKing() && health + 4 <= 200) {
				health += 4;
			}
			timeAddHealth = Time.time;
			TranslateChildHealthBar();
		}

	}




	void CreateSpriteArray () {
		for (int i = 0; i < playerSprite.Length/2; i++) {
			playerSprite[i] = Resources.Load<Sprite>("SpritePlayer/RotateLeft/Player" + (i + 1));
		}

		for (int i = playerSprite.Length/2; i < playerSprite.Length; i++) {
			playerSprite[i] = Resources.Load<Sprite>("SpritePlayer/RotateRight/Player" + (i - 6));
		}
	}


	void DisableLockEnemyComponent () {
		if (RadaLockEnemy.Instant != null) {
			RadaLockEnemy.Instant.DeActiveLockEnemy();
		}
	}


	void TranslateChildHealthBar() {
		if(IsTheKing()) {
			ScaleChildHealthBar(2);
		}
		else { ScaleChildHealthBar(1); }
			
	}

	void ScaleBorderHealthBarWhenBeTheKingOrNot () {
		if (IsTheKing() && Mathf.Approximately(borderHealth.GetComponent<SpriteRenderer>().bounds.size.x, borderHealthLengthWhenStart)) {
			ScaleHealthBarWhenBeTheKing();
			ScalePlayerWhenBeTheKing();
			ScaleChildHealthBar(2);
		}
		else if (!IsTheKing() && Mathf.Approximately(borderHealth.GetComponent<SpriteRenderer>().bounds.size.x, borderHealthLengthWhenStart * 2)) {
			ReturnPlayerFromKing();
			ScalePlayerNormal();
			ScaleChildHealthBar(1);
		}
	}

	void ScalePlayerWhenBeTheKing() {
		transform.localScale = new Vector3(2.2f, 2.2f, 2.2f);
	}
		
	void ScalePlayerNormal() {
		transform.localScale = new Vector3(2, 2, 2);
	}

	void ScaleChildHealthBar (int index) {
		if(health >= 0 && health <= HEALTHSTART * index && state != PlayerState.Break) {
			childHealthBar.transform.localScale =  new Vector3(health/HEALTHSTART, 1, 1);
			Vector3 locVec = new Vector3(parentHealthBar.transform.position.x - ((HEALTHSTART * index - health)/2 
				* (borderHealthLengthWhenStart/(HEALTHSTART * index))) 
				* index, parentHealthBar.transform.position.y, parentHealthBar.transform.position.z);

			childHealthBar.transform.position = locVec;

			if(health >= 60 * index)
				childHealthBar.GetComponent <SpriteRenderer>().color = new Color(29.0f/255, 148.0f/255, 40.0f/255);
			else if(health > 30 * index && health < 60 * index)
				childHealthBar.GetComponent<SpriteRenderer>().color = new Color(234.0f/255, 112.0f/255, 112.0f/255);
			else if(health <= 30 * index)
				childHealthBar.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);
		}
	}


	void Translate() {
		if (baseJoyStick.canMove) {
			if (state == PlayerState.MovingSlow) {
				speedSlowMoving = Mathf.Lerp(speedSlowMoving, 0, 0.02f);
				transform.Translate(0, speedSlowMoving, 0);

				if (speedSlowMoving < 0.1f * speedMoving)
					Idle();
			}
			else if (state == PlayerState.FallDown) {
				speedSlowMoving = Mathf.Lerp(speedSlowMoving, 0, 0.03f);
				transform.Translate(0, speedSlowMoving, 0);
				if (speedSlowMoving < 0.01f * speedMoving)
					speedSlowMoving = 0;
			}
			else if (state != PlayerState.Idle) {
					transform.Translate(0, speedMoving, 0);
			}
		}
	}

	void Rotate () {
		if (canRotate) {
			Vector2 movement = baseJoyStick.GetMovement ();
			if (movement.sqrMagnitude > 0) {
				float alpha = -90 + Mathf.Atan2 (movement.y, movement.x) * Mathf.Rad2Deg;
				desRotation = Quaternion.Euler (new Vector3 (0, 0, alpha));
			}
			transform.rotation = Quaternion.Lerp (transform.rotation, desRotation, rotationSpeed * Time.deltaTime);
		}
	}
		
	void HealthBarFollowPlayer() {
		if (state == PlayerState.Break)
			parentHealthBar.SetActive(false);
		else
			parentHealthBar.transform.position = new Vector3(transform.position.x, transform.position.y - 0.7f, transform.position.z);	
	}


	public void ReturnNormalBullet() {
		if (totalBullet == 0) {
			if(weapon == WeaponType.Rocket) {
				DisableRada();
			}
			ChangeWeapon(WeaponType.Normal);
			Color normalColor = new Color(255, 255, 255);
			ChangeTailColor(normalColor);

			Sprite spr = Resources.Load<Sprite>("Image/PowerUp/1");
			weaponImageInfo.sprite = spr;
			txtWeaponLeft.text = "∞";
		}
	}
		
	private IEnumerator SpeedUpWhenHaveCoin() {
		speedMoving = speedWhenStart * 1.2f;
		head.SetActive(true);
		yield return new WaitForSeconds(1.0f);
		speedMoving = speedWhenStart;
		head.SetActive(false);
	}

	void CreateComponentAfterDie () {
		switch (weapon) {
		case WeaponType.Boom:
			GameObject boom = ComponentControl.Instant.GetBoomComponent();
			boom.transform.position = transform.position;
			break;


		case WeaponType.ThreeBullet:
			GameObject threeBullet = ComponentControl.Instant.GetThreeBulletComponent();
			threeBullet.transform.position = transform.position;
			break;

		case WeaponType.Rocket:
			GameObject rocket = ComponentControl.Instant.GetRocketComponent();
			rocket.transform.position = transform.position;
			break;

		case WeaponType.Punch:
			GameObject punch = ComponentControl.Instant.GetPunchComponent();
			punch.transform.position = transform.position;
			break;

		case WeaponType.Lightning:
			GameObject lightning = ComponentControl.Instant.GetLightningComponent();
			lightning.transform.position = transform.position;
			break;
		}
	}

	public void ShowWeaponLeft() {
		if(weapon == WeaponType.Normal)
			txtWeaponLeft.text = "∞";
		else
			txtWeaponLeft.text = totalBullet.ToString();
	}

	public void SpeedUpByPunch () {
		GetComponent<Rigidbody2D> ().AddForce(transform.up * speedPunch);
	}


	public void BeginSlow () {
		state = PlayerState.MovingSlow;
		speedSlowMoving = speedMoving;

	}

	public void BeginFly() {
		state = PlayerState.Fly;
	}

	void Idle() {
		state = PlayerState.Idle;
		baseJoyStick.SetCanMove(false);
	}

	public void ChangeRotation(bool rotate)	{
		canRotate = rotate;
	}


	public void ChangeHavePunch(bool have) {
		havePunch = have;
	}
		

	bool create3PartOneTime = false;
	void Create3PartAfterBreak() {
		if (!create3PartOneTime) {
			GameObject[] threePart = new GameObject[3];
			for (int i = 0; i < 3; i++) {
				threePart[i] = ComponentControl.Instant.GetOneOf3PartAfterBreak();
				threePart[i].transform.position = transform.position;
				AddForceForOneOf3Part(threePart[i]);
			}
			create3PartOneTime = true;
		}

	}


	void CreateBreakComponent () {
		GameObject breakComponent = ComponentControl.Instant.GetBreakComponent();
		breakComponent.transform.position = transform.position;
		timeLoadNewGame = breakComponent.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
	}



	void AddForceForOneOf3Part(GameObject target) {
		target.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f,1f), Random.Range(-1f,1f)).normalized * 150);
	}





	public bool isOutOfUpBorder() {
		if (transform.position.y > heightborder)
			return true;
		return false;
	}


	public bool isOutOfBottomBorder() { 
		if (transform.position.y < -heightborder - 0.8f) 
			return true;
		return false;
	}


	public bool isOutOfRightLeftBorder() {
		if (Mathf.Abs (transform.position.x) > widthborder)
			return true;
		return false;
	}
		
	void CreateAddScoreTxt (string txt) {
		GameObject addScoreTxt = ComponentControl.Instant.GetAddScoreTxt();
		addScoreTxt.transform.parent = GameObject.FindGameObjectWithTag("CanvasMain").transform;
		addScoreTxt.transform.localScale = Vector3.one;
		addScoreTxt.transform.localPosition = new Vector3 (0, 100, 0);
		addScoreTxt.GetComponent<Text>().text = txt;
	}
		
	void CollisionWithEnemy () {
		StartCoroutine(ShakePlayer());
	}

	IEnumerator ShakePlayer() {
		float rotZ = transform.eulerAngles.z;
		transform.rotation = Quaternion.Euler(0, 0, rotZ - 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ + 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ - 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ + 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ - 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ);
	}


	float timeEscapeSea;
	public bool isUnderSea {get; private set;}
	void OnTriggerEnter2D (Collider2D col)	{
		string tag = col.tag;
		Color materialColor = new Color();
		Sprite spr;
		if (state != PlayerState.Break) {
			if(tag == "3Bullet" || tag == "Boom" || tag == "Lightning" || tag == "Punch" || tag == "Rocket" || tag == "Health") {
				score += 10;
				CreateAddScoreTxt("+10");
			}
				
			if (!havePunch && !isUnvunerable) {
				switch (tag) {
					case "EBullet":
						health -= 20;
						if(health <= 0) {
							health = 0;
							BeKilledBySomeoneActive(col.GetComponent<BulletOfEnemy>().GetOwnEnemy());
						}
						Destroy(col.gameObject);
						break;

					case "Enemy":
						if(state != PlayerState.Break) {
							health = health - 90;
							if(health <= 0) {
								health = 0;
								BeKilledBySomeoneActive(col.gameObject.GetComponent<Enemy>());
							} 
							CollisionWithEnemy();
						}
						else if(state == PlayerState.FallDown) {
							health = 0;
						}
						break;

					case "EBoom":
						health -= 100;
						if(health <= 0) {
							health = 0;
							BeKilledBySomeoneActive(col.GetComponent<EBoom>().GetOwnEnemy());
						}

						Destroy(col.gameObject);
						break;

					case "EPunch":
						health -= 100;
						if(health <= 0) {
							health = 0;
							BeKilledBySomeoneActive(col.GetComponent<EPunch>().GetOwnEnemy());
						}
						break;

				case "ELightning":
					health -= 80;
					if(health <= 0) {
						health = 0;
						BeKilledBySomeoneActive(col.GetComponent<ELightning>().GetOwnEnemy());
					}
					break;

				case "ERocket":
					health -= 100;
					if(health <= 0) {
						health = 0;
						BeKilledBySomeoneActive(col.GetComponent<ERocket>().GetOwnEnemy());

					}
					Destroy(col.gameObject);
					break;

				}
				TranslateChildHealthBar();
			}
				

		switch(tag) {
			case "Coin":
				score = score + 5;
				StartCoroutine(SpeedUpWhenHaveCoin());
				CreateAddScoreTxt("+5");
				break;

			case "Health":
				if(IsTheKing()) {
					if (health + 100 <= 200)
						health += 100;
					else 
						health = 200;
				}
				else { 
					health = 100; 
				}
				TranslateChildHealthBar();
				col.gameObject.SetActive (false);
				break;

			case "Boom":
				if (weapon == WeaponType.Boom) {
					totalBullet += 10;
				}
				else {
					ChangeWeapon(WeaponType.Boom);
					totalBullet = 10;
				}
				DisableRada();
				materialColor = new Color(0.32f,0.09f,0.09f);
				ChangeTailColor(materialColor);
				col.gameObject.SetActive (false);
				spr = Resources.Load<Sprite>("Image/PowerUp/2");
				weaponImageInfo.sprite = spr;
				break;

			case "3Bullet":
				if (weapon == WeaponType.ThreeBullet) {
					totalBullet += 300;
				}else {
					ChangeWeapon(WeaponType.ThreeBullet);
					totalBullet = 300;
				}
				DisableRada();
				materialColor= new Color(0.94f,0.45f,0.19f);
				ChangeTailColor(materialColor);
				col.gameObject.SetActive (false);
				spr = Resources.Load<Sprite>("Image/PowerUp/3");
				weaponImageInfo.sprite = spr;
				break;

			case "Rocket":
				if (weapon == WeaponType.Rocket) {
					totalBullet += 10;
				}else {
					ChangeWeapon(WeaponType.Rocket);
					totalBullet = 10;
				}
				EnableRada();
				materialColor= new Color(0.9f,0,0);
				ChangeTailColor(materialColor);
				col.gameObject.SetActive (false);
				spr = Resources.Load<Sprite>("Image/PowerUp/4");
				weaponImageInfo.sprite = spr;
				break;
			
			case "Punch":
				if (weapon == WeaponType.Punch) {
					totalBullet += 10;
				}else {
					ChangeWeapon(WeaponType.Punch);
					totalBullet = 10;
				}
				DisableRada();
				materialColor= new Color(0.8f,0.08f,0.92f);
				ChangeTailColor(materialColor);
				col.gameObject.SetActive (false);
				spr = Resources.Load<Sprite>("Image/PowerUp/5");
				weaponImageInfo.sprite = spr;
				break;

			case "Lightning":
				if (weapon == WeaponType.Lightning) {
					totalBullet += 10;
				}else {
					ChangeWeapon(WeaponType.Lightning);
					totalBullet = 10;
				}
				DisableRada();
				materialColor= new Color(0.2f,0.35f,0.86f);
				ChangeTailColor(materialColor);
				col.gameObject.SetActive (false);
				spr = Resources.Load<Sprite>("Image/PowerUp/6");
				weaponImageInfo.sprite = spr;
				break;
			
			case "UpSea":
				GameObject upSea = ComponentControl.Instant.getTouchSea();
				float anglez = transform.rotation.eulerAngles.z;

				if(anglez >90 && anglez<270) {
					isUnderSea = true;
					upSea.transform.position = transform.position;
					health -= 20;
					if(health <= 0) {
						health = 0;
					}

					if(anglez > 135 && anglez < 180) {
						transform.rotation = Quaternion.identity;
						transform.RotateAround(transform.position, Vector3.forward, anglez - 30);
					}
					else if(anglez > 180 && anglez < 225) {
						transform.rotation = Quaternion.identity;
						transform.RotateAround(transform.position, Vector3.forward, anglez + 30);

					}
					canRotate = false;
					TranslateChildHealthBar();
				}
				else {
					if (state == PlayerState.FallDown) {
						health = 0;
					}
					isUnderSea = false;
					timeEscapeSea = Time.time;
					float angleTemp = transform.rotation.eulerAngles.z;
					if(angleTemp > 0 && angleTemp < 90 ) {
						transform.rotation = Quaternion.identity;
						transform.RotateAround(transform.position, Vector3.forward, angleTemp - 30);
					}
					else if(angleTemp > 270 && angleTemp < 360 ) {
						transform.rotation = Quaternion.identity;
						transform.RotateAround(transform.position, Vector3.forward, angleTemp + 30);
					}
					upSea.transform.position = new Vector3(transform.position.x, transform.position.y , transform.position.z);
				}
				break;

			}

			ChangeWeaponInfo(totalBullet);
		}

	}


	void EnableRada() {
		if(radaOfPlayer.activeInHierarchy == false) {
			radaOfPlayer.SetActive(true);
		}
	}


	void DisableRada() {
		if (radaOfPlayer.activeInHierarchy == true) {
			RadaLockEnemy.Instant.DeActiveLockEnemy();
			RadaLockEnemy.Instant.SetNullEnemy();
			radaOfPlayer.SetActive(false);
		}
	}


	public void ScaleHealthBarWhenBeTheKing() {
		health *= 2;
		borderHealth.transform.localScale = new Vector3(2,1,0);
		childHealthBar.transform.localScale = new Vector3(2,1,0);
	}

	public bool IsTheKing() {
		if(ShowInfo.Show.GetKingOfMap() == gameObject)
			return true;
		return false;
	}


	public void ReturnPlayerFromKing() {
		health /= 2;
		borderHealth.transform.localScale = new Vector3(1,1,0);
		childHealthBar.transform.localScale = new Vector3(1,1,0);
	}
		

	public void PlayerKillEnemy(GameObject enemy) {
		if (state == PlayerState.FallDown) {
			speedMoving = speedWhenStart;
			GetComponent<Rigidbody2D>().gravityScale = 0;
			GetComponent<Rigidbody2D>().AddForce(transform.up * 50);
		}

		score += 20;
		if(IsTheKing()) {
			if(health + 20  <= 200)
				health += 20;
			else
				health = 200;
		}
		else {
			if(health + 10  <= 100)
				health += 10;
			else
				health = 100;
		}
		TranslateChildHealthBar();
		CreateAddScoreTxt("+20");
		SetTextKillSomoneActive(enemy);

	} 



	public void SetTextKillSomoneActive(GameObject enemy) {
		killSomeone.SetTimeShowInfo();
		killSomeone.SetKillInfo("You killed " + enemy.GetComponent<Enemy>().nameEnemy);
		killSomeone.SetColor(new Color(9/255f, 1, 0));
		if(!killSomeone.gameObject.activeSelf)
			killSomeone.gameObject.SetActive(true);
	}


	public void BeKilledBySomeoneActive(Enemy enemy) {
		killSomeone.SetTimeShowInfo();
		killSomeone.SetKillInfo("Killed by " + enemy.nameEnemy);
		killSomeone.SetColor(new Color(233/255f, 125/255f, 118/255f));
		if(!killSomeone.gameObject.activeSelf)
			killSomeone.gameObject.SetActive(true);
	}


	void ChangeTotalBullet(int tol)	{
		totalBullet = tol;
	}


	void ChangeWeaponInfo(int weaponLeft) {
		if (weaponLeft != -10)
			txtWeaponLeft.text = weaponLeft.ToString();
	}


	public void DecreaseBullet(int a) {
		totalBullet -= a;
		if (totalBullet < 0)
			totalBullet = 0;
	}


	public bool isEscapeSea() {
		if (timeEscapeSea == 0)
			return true;
		else if (timeEscapeSea != 0 && Time.time - timeEscapeSea > 0.5f)
			return true;
		
		else return false;
	}


	public bool underTheSea() {
		return isUnderSea;
	}


	void OnTriggerStay2D (Collider2D col) {
		if(col.tag == "UpSea") {
			GameObject upSea = ComponentControl.Instant.getTouchSea();
			upSea.transform.position = transform.position;
		}
	}



	void ChangeWeapon(WeaponType ChangeTo) {
		weapon = ChangeTo;
	}
		

	public float GetAngle() {
		return this.angle;
	}


	public float GetSpeedMoving() {
		return speedMoving;
	}


	public void SetSpeedMoving(float speed) {
		speedMoving = speed;
	}


	public  void ChangeAngle(float angle1) {
		angle = angle1;
	}


	float time = 0;
	public void invulreable () {
		if(Time.time - time > 0.1f) {
			gameObject.GetComponent<SpriteRenderer>().enabled = true;
			time = Time.time;
		}
		else
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
	}


	public void ChangeTailColor(Color col) {
		tailMaterial.SetColor("_Color", col);
	}


	bool tempCheck = true;
	bool justOutUp = false;
	void controlOutOfScreen() {
		float angleTemp = transform.rotation.eulerAngles.z;
		if(isOutOfBottomBorder()) {
			if(state ==PlayerState.Fly) {
				transform.rotation = Quaternion.identity;
				if(angleTemp > 90 && angleTemp < 180)
					transform.RotateAround(transform.position, Vector3.forward, 180 - angleTemp);
				else if (angleTemp > 180 && angleTemp < 270)
					transform.RotateAround(transform.position, Vector3.forward, 540 - angleTemp);
				
				transform.GetComponent<Rigidbody2D>().AddForce(transform.up * 300);
			}
			else if(state== PlayerState.FallDown) {
					health=0;
				 }
		}


		if(isOutOfRightLeftBorder() && tempCheck) {
			timeOutOfRightLeft = Time.time;
			txtNotification.gameObject.SetActive(true);
			tempCheck = false;
		}
		else if (!isOutOfRightLeftBorder()){
			txtNotification.gameObject.SetActive(false);
			timeOutOfRightLeft = 0;
			tempCheck = true;
		}


		if(isOutOfUpBorder()) {
			TranslateOutOfUpBorder();
			justOutUp = true;
		}
		else {
			if (justOutUp && baseJoyStick.canMove) {
				velocityUpBorder -= 0.6f * Time.deltaTime;
				transform.Translate(0, -velocityUpBorder, 0, Space.World);
				if(velocityUpBorder <= 0.000001f) {
					justOutUp = false;
				}
			}
		}
	}



	void TranslateOutOfUpBorder() {
		float a = force / mass;
		if (velocityUpBorder < speedWhenStart * 2) {
			velocityUpBorder += a * 0.25f * Time.deltaTime;
			velocityUpBorder = Mathf.Clamp(velocityUpBorder, 0, 0.8f);
		}
		transform.Translate(0, -velocityUpBorder, 0, Space.World);
	}




	public void SetState (PlayerState st) {
		state = st;
	}



	public void SetHealth(int heal) {
		health = heal;
	}



	public void PlayAgain() {
		isDie = false;
		score = score/2;
		weapon = WeaponType.Normal;
		state = PlayerState.Idle;
		gameObject.SetActive(true);
		parentHealthBar.SetActive(true);
		trailRender.SetActive(false);
		isUnvunerable = true;
		StartCoroutine(EscapeUnvunerable());
		timePlayGame = Time.time;
		childHealthBar.transform.position = Vector3.zero;
		circleAround.SetActive(true);
		tail.SetActive(true);
		killSomeone.gameObject.SetActive(false);
		isUnderSea = false;
		create3PartOneTime = false;
		transform.rotation = Quaternion.identity;
		desRotation = Quaternion.identity;
		circleAround.transform.rotation = Quaternion.identity;
		killSomeone.gameObject.SetActive(false);
		ChangeWeapon(WeaponType.Normal);
		Color normalColor = new Color(255, 255, 255);
		ChangeTailColor(normalColor);
		DisableRada();

		Sprite spr = Resources.Load<Sprite>("Image/PowerUp/1");
		weaponImageInfo.sprite = spr;
		txtWeaponLeft.text = "∞";

		if(IsTheKing()) {
			health = 200;
			ScaleChildHealthBar(2);
		}
		else {
			health = 100;
			ScaleChildHealthBar(1);
		}
			
	}

	public bool IsUnvunerable () {
		return isUnvunerable;
	}


	void CreateTouchSeaWhenUpSurface () {
		float distance = transform.position.y - upSea.transform.position.y;
		if (distance <= 3f && distance >= 0) {
			GameObject touchSea = ComponentControl.Instant.getTouchSea();
			touchSea.transform.position = new Vector3(transform.position.x, upSea.transform.position.y, transform.position.z);
		}
	}



}
