﻿using UnityEngine;
using System.Collections;

public class SyncPlayerAndComponent : MonoBehaviour {

	public GameObject mainCanvas;
	public GameObject dieCanvas;

	public GameObject circleAroundPlayer;
	public GameObject head;
	public GameObject particleObject;

	public void DeActiveWhenDie() {
		mainCanvas.SetActive(false);
		circleAroundPlayer.SetActive(false);
		head.SetActive(false);
	}

	public void DeActiveWhenBreak() {
		circleAroundPlayer.SetActive(false);
		head.SetActive(false);
	}


	public void ActiveAll() {
		mainCanvas.SetActive(true);
		circleAroundPlayer.SetActive(true);
	}

}
