﻿using UnityEngine;
using System.Collections;
using System;

public class CircleAroundPlayer : MonoBehaviour {

	public GameObject trangle;
	public MyJoyStick baseJoyStick;
	public Player player;

	Quaternion desRotation;
	float rotationSpeed = 20;


	void Start () {
		trangle.transform.position = new Vector3(transform.position.x, transform.position.y+1.4f, transform.position.z);
		transform.position = player.transform.position;
	}
	

	void Update () {
		CircleFollowPlayer();
		RotateTrangle();
	}


	void CircleFollowPlayer() {
		transform.position = player.transform.position;
	}


	void RotateTrangle() {
		if(player.state == Player.PlayerState.Fly || player.state == Player.PlayerState.Idle 
			|| player.state == Player.PlayerState.MovingSlow) {

			Vector2 movement = baseJoyStick.GetMovement();
			if (movement.sqrMagnitude > 0) {
				float alpha = -90 + Mathf.Atan2(movement.y, movement.x) * Mathf.Rad2Deg;
				desRotation = Quaternion.Euler(new Vector3(0, 0, alpha));
				transform.rotation = Quaternion.Lerp(transform.rotation, desRotation, rotationSpeed * Time.deltaTime);

				trangle.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
				trangle.transform.localPosition = new Vector3(0, 2, 0);
			}


		}


		if(!baseJoyStick.down) {
			trangle.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
			trangle.transform.localPosition = new Vector3(0, 1.5f, 0);			
		}
	}


}
