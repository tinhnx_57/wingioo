﻿using UnityEngine;
using System.Collections;

public class RadaLockEnemy : MonoBehaviour {
	public Player player;
	public GameObject lockEnemyPrefab;

	public bool killedEnemy { get; private set; }
	Enemy enemy;
	GameObject lockEnemy;
	bool choseOneEnemy;
	float timeDetectEnemy;
	bool lockedTargetEnemy;

	static RadaLockEnemy radaLock;
	void Awake() {
		radaLock = this;
	}

	public static RadaLockEnemy Instant {
		get {
			return radaLock;
		}
	}

	void Start() {
		lockedTargetEnemy = false;
		CreateLockEnemy();
	}


	void Update () {
		RadaFollowPlayer();
		if(DetectedEnemy()) {
			LockFollowEnemy(enemy.gameObject);
		}
		else {
			DeActiveLockEnemy();
			choseOneEnemy = false;
		}
	}


	void RadaFollowPlayer()	{
		transform.rotation = player.transform.rotation;
		transform.position = player.transform.position + player.transform.up * 0.5f;
	}



	bool DetectedEnemy() {
		if(enemy != null && enemy.state != Enemy.State.Break)
			return true;
		return false;
	}


	bool HadLockEnemy()	{
		if(lockEnemy.activeInHierarchy == true)
			return true;
		return false;
	}



	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Enemy" && !choseOneEnemy) { // chose one enemy in box collider
			enemy = col.gameObject.GetComponent<Enemy>();
			choseOneEnemy = true;
			lockEnemy.SetActive(true);
			changeLockColor(lockEnemy, new Color(1,1,1));
			timeDetectEnemy = Time.time;
		}
	}


	void OnTriggerExit2D(Collider2D col) {
		if(DetectedEnemy() && col.gameObject == enemy.gameObject && !lockedTargetEnemy) {
			choseOneEnemy = false;
			enemy = null;
			DeActiveLockEnemy();
		}
	}


	void OnTriggerStay2D(Collider2D col) {
		if(DetectedEnemy())	{
			if(col.gameObject == enemy.gameObject) {
				if(timeDetectEnemy != 0 && (Time.time - timeDetectEnemy) >= 0.4f && !lockedTargetEnemy) {
					changeLockColor(lockEnemy, new Color(242/255f, 5/255f, 5/255f));
					lockedTargetEnemy = true;
				}
				if(enemy.state == Enemy.State.Break) {
					enemy = null;
					lockEnemy.SetActive(false);
					lockedTargetEnemy = false;
				}
			}
		}
	}


	void changeLockColor (GameObject lockObject, Color color) {
		lockObject.GetComponent<SpriteRenderer>().color = color;
	}
		

	void CreateLockEnemy() {
		lockEnemy = Instantiate(lockEnemyPrefab, transform.position, Quaternion.identity) as GameObject;
		lockEnemy.SetActive(false);
	}
		

	void LockFollowEnemy(GameObject e) {
		lockEnemy.transform.position = e.transform.position;
	}


	public void DeActiveLockEnemy() {
		lockEnemy.SetActive(false);
	}


	public Enemy GetTargetEnemy() {
		if (lockedTargetEnemy) {
			return  enemy;
		}
		else return null;
	}



	public void SetNullEnemy () {
		enemy = null;
	}


	public void ChangeLockTarget (bool change) {
		lockedTargetEnemy = change;
	}




}
