﻿using UnityEngine;
using System.Collections;

public class RadaCatchEnemy : MonoBehaviour {
	public Player player;


	void Update () {
		RadaFollowPlayer();
	}


	void RadaFollowPlayer() {
		transform.rotation = player.transform.rotation;
		transform.position = player.transform.position + player.transform.up * 0.5f;
	}


	void OnTriggerExit2D(Collider2D col) {
		if(col.tag == "Enemy" && col.GetComponent<Enemy>() == RadaLockEnemy.Instant.GetTargetEnemy()) {
			RadaLockEnemy.Instant.ChangeLockTarget(false);
			RadaLockEnemy.Instant.SetNullEnemy();
			RadaLockEnemy.Instant.DeActiveLockEnemy();
		}
	}


	void OnTriggerStay2D(Collider2D col) {
		if(col.tag == "Enemy" && col.GetComponent<Enemy>() == RadaLockEnemy.Instant.GetTargetEnemy()) {
			if(col.GetComponent<Enemy>().state == Enemy.State.Break) {
				RadaLockEnemy.Instant.ChangeLockTarget(false);
				RadaLockEnemy.Instant.SetNullEnemy();
				RadaLockEnemy.Instant.DeActiveLockEnemy();
			}


		}

	}

}
