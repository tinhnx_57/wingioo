﻿using UnityEngine;
using System.Collections;

public class Tail : MonoBehaviour {
	public Player player;


	void Start () {
		transform.position = player.transform.position;
	}
	

	void Update () {
		transform.rotation = player.transform.rotation;
		transform.position = player.transform.position - player.transform.up * 0.4f;
		transform.position = new Vector3(transform.position.x, transform.position.y , transform.position.z + 2);
		if(player.state == Player.PlayerState.Break)
			gameObject.SetActive(false);

	}




}
