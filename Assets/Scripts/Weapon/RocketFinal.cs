﻿using UnityEngine;
using System.Collections;

public class RocketFinal : MonoBehaviour {
	public float rocketSpeed;

	Enemy targetEnemy;
	Vector3 startPosition;


	void Start () {
		targetEnemy = RadaLockEnemy.Instant.GetTargetEnemy();
		startPosition = transform.position;
	}



	void Update () {
		MovingFollowTarget(targetEnemy);
		DestroyByDistance();
	}


	void MovingFollowTarget(Enemy target) {
		if(target != null) {
			Quaternion rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, 0.2f);
			Vector2 lerpPos = Vector2.Lerp(transform.position, target.transform.position, 0.2f);
			transform.rotation = rotation;
			transform.position = lerpPos;
		}
		else {
			transform.Translate(0, rocketSpeed, 0);
		}
			
		if(isOutOfScreen(gameObject)) {
			Destroy(gameObject);
		}
	}


	void DestroyByDistance() {
		if(Vector3.Distance(startPosition, transform.position) >= 20) {
			GameObject breakCpn = ComponentControl.Instant.GetBreakComponent();
			breakCpn.transform.position = gameObject.transform.position;
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Enemy") {
			RadaLockEnemy.Instant.SetNullEnemy();
			Destroy(gameObject);

		}
	}

	public bool isOutOfScreen(GameObject obj) {
		if(Mathf.Abs(obj.transform.position.x) > Background.Instant.boundOfBackgroundX() || 
			Mathf.Abs(obj.transform.position.y) > Background.Instant.boundOfBackgroundX())
			return true;
		return false;
	}

}
