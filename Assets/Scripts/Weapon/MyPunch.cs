﻿using UnityEngine;
using System.Collections;

public class MyPunch : MonoBehaviour {
	private Player player;


	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	}
	

	void Update () {
		if (player.weapon == Player.WeaponType.Punch) {
			transform.rotation = player.transform.rotation;
			transform.position = player.transform.position;
		}
		else Destroy(gameObject);

	}
}
