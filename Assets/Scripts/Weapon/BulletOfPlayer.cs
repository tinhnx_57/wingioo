﻿using UnityEngine;
using System.Collections;

public class BulletOfPlayer : MonoBehaviour {
	
	public float movingSpeed;

	Vector2 startPostion;
	Player player;
	float timeStart;
	float angleMoving;
	float distanceToDestroy;

	void Start () {
		distanceToDestroy = 8.0f;
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		startPostion = player.transform.position;
		timeStart = Time.time;
	}
	

	void Update () {
		movingBullet();
		DestroyBulletByDistance();
		DestroyBulletOutOfScreen();


	}


	public void movingBullet() {
		transform.Translate(0, movingSpeed, 0);

	}


	void DestroyBulletOutOfScreen()	{
		if(Mathf.Abs(transform.position.x) > Background.Instant.boundOfBackgroundX() || 
			Mathf.Abs(transform.position.y) > Background.Instant.boundOfBackgroundY())
			Destroy(gameObject);
	}


	void DestroyBulletByDistance () {
		if(Vector2.Distance(startPostion, transform.position) >= distanceToDestroy) {
			Destroy(gameObject);
		}
	}



	public void setStartPosition (Vector3 pos) {
		startPostion = pos;
	}

}
