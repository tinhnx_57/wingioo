﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {
	
	public float speedRocket;
	Vector3 startPosition;
	Enemy enemy;
	bool onlyOne;


	void Start() {
		onlyOne = false;
		startPosition = transform.position;
	}


	void Update () {
		transform.Translate(0,speedRocket,0);
		if(enemy != null) {
			movingFollowEnemy();
			checkDistance();

		}

		if(isOutOfScreen(gameObject)) 
			Destroy(gameObject);
	}


	void OnTriggerEnter2D(Collider2D col) {
		string tag = col.tag;
		if(tag == "Enemy" && !onlyOne) {
			enemy = col.GetComponent<Enemy>();
			onlyOne =true;
		}
	}

	//check enemy position relative with vector up of rocket
	public float getPosRelativeWithRocketVector(Vector2 pos, Vector2 vecB1, Vector2 vecB2) {
		return (pos.x-vecB1.x) * (vecB2.y - vecB1.y) - (pos.y-vecB1.y) * (vecB2.x - vecB1.x);

	}


	void movingFollowEnemy() {
		
		Vector2 vecA = transform.up;
		Vector2 rocketPos = new Vector2(transform.position.x, transform.position.y);

		Vector2 enemyPos = new Vector2(enemy.transform.position.x, enemy.transform.position.y);
		Vector2 vecB = new Vector2(enemy.transform.position.x- transform.position.x, enemy.transform.position.y - transform.position.y);

		float cosAlpha = (vecA.x * vecB.x + vecA.y* vecB.y) / (vecA.magnitude * vecB.magnitude);
		float Alpha = Mathf.Acos(cosAlpha) * Mathf.Rad2Deg;


		Vector2 temp = rocketPos + vecA;

		if(!float.IsNaN(Alpha))	{
			float checkVar;

			checkVar = getPosRelativeWithRocketVector(enemyPos, rocketPos, temp);

			if(checkVar > 0)
				Alpha = 360 - Alpha;
				
			transform.RotateAround(transform.position, transform.forward, Alpha);
			speedUpToKissEnemy();
			Vector3 lerpPos = Vector3.Lerp(transform.position, enemy.transform.position, 0.1f);
			transform.position = lerpPos;

		}
	}
		

	void checkDistance() {
		if(Vector2.Distance(transform.position, enemy.transform.position) <= 0.25f)
			Destroy(gameObject);
	}

	public void speedUpToKissEnemy() {
		speedRocket = speedRocket * 1.08f;
	}



	public bool isOutOfScreen(GameObject obj) {
		if(Mathf.Abs(obj.transform.position.x)> Background.Instant.boundOfBackgroundX() || 
			Mathf.Abs(obj.transform.position.y) > Background.Instant.boundOfBackgroundX())
			return true;
		return false;
	}



}

