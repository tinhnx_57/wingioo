﻿using UnityEngine;
using System.Collections;

public class ComponentControl : MonoBehaviour {

	static ComponentControl component;


	#region ComponentHaveBallon
	public GameObject bulletPrefab;
	public GameObject enemyPrefab;
	public GameObject boomPrefab;
	public GameObject coinPrefab;
	public GameObject lightningPrefab;
	public GameObject rocketPrefab;
	public GameObject punchPrefab;
	#endregion

	public GameObject touchSea;
	public GameObject eBullet;
	public GameObject eRocket;
	public GameObject ePunch;
	public GameObject eBoom;
	public GameObject eLightning;
	public GameObject healthBar;
	public GameObject kingHat;
	public GameObject oneOf3PartAfterBreak;
	public GameObject addScoreTxt;
	public GameObject breakComponent;

	#region ComponentVar
	public GameObject boomComponent;
	public GameObject threeBulletComponent;
	public GameObject rocketComponent;
	public GameObject punchComponent;
	public GameObject lightningComponent;
	#endregion

	void Awake() {
		component = this;
	}

	public static ComponentControl Instant {
		get {return component;}
	}


	public GameObject GetCoin()	{
		GameObject obj = Instantiate(coinPrefab) as GameObject;
		return obj;
	}
	public GameObject GetBullet() {
		GameObject obj = Instantiate(bulletPrefab) as GameObject;
		return obj;
	}

	public GameObject GetEnemy() {
		GameObject obj = Instantiate(enemyPrefab) as GameObject;
		return obj;
	}

	public GameObject GetBoom()	{
		GameObject obj = Instantiate(boomPrefab) as GameObject;
		return obj;
	}

	public GameObject GetRocket() {
		GameObject obj = Instantiate(rocketPrefab) as GameObject;
		return obj;
	}

	public GameObject GetLightning() {
		GameObject obj = Instantiate(lightningPrefab) as GameObject;
		return obj;
	}

	public GameObject GetPunch() {
		GameObject obj = Instantiate(punchPrefab) as GameObject;
		return obj;
	}
		

	public GameObject getTouchSea()	{
		GameObject obj = Instantiate(touchSea) as GameObject;
		return obj;
	}



	public GameObject getEBullet() {
		GameObject obj = Instantiate(eBullet) as GameObject;
		return obj;
	}

	public GameObject getEBoom() {
		GameObject obj = Instantiate(eBoom) as GameObject;
		return obj;
	}



	public GameObject getERocket()	{
		GameObject obj = Instantiate(eRocket) as GameObject;
		return obj;
	}


	public GameObject getEPunch() {
		GameObject obj = Instantiate(ePunch) as GameObject;
		return obj;
	}


	public GameObject getELightning() {
		GameObject obj = Instantiate(eLightning) as GameObject;
		return obj;
	}


	public GameObject getHealthBar() {
		GameObject obj = Instantiate(healthBar) as GameObject;
		return obj;
	}
		


	public GameObject GetKingHat() {
		GameObject obj = Instantiate(kingHat) as GameObject;
		return obj;
	}



	public GameObject GetOneOf3PartAfterBreak() {
		GameObject obj = Instantiate(oneOf3PartAfterBreak) as GameObject;
		return obj;
			
	}



	public GameObject GetBreakComponent () {
		GameObject obj = Instantiate(breakComponent) as GameObject;
		return obj;
	}


	public GameObject GetAddScoreTxt () {
		GameObject obj = Instantiate(addScoreTxt) as GameObject;
		return obj;
	}



	#region createComponent

	public GameObject GetBoomComponent () {
		GameObject ob = Instantiate(boomComponent) as GameObject;
		return ob;
	}

	public GameObject GetThreeBulletComponent () {
		GameObject ob = Instantiate(threeBulletComponent) as GameObject;
		return ob;
	}


	public GameObject GetRocketComponent () {
		GameObject ob = Instantiate(rocketComponent) as GameObject;
		return ob;
	}

	public GameObject GetPunchComponent () {
		GameObject ob = Instantiate(punchComponent) as GameObject;
		return ob;
	}


	public GameObject GetLightningComponent () {
		GameObject ob = Instantiate(lightningComponent) as GameObject;
		return ob;
	}


	#endregion

}