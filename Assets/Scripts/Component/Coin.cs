﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {
	public int index {get; private set;}
	public static bool collected {get; private set;}
	GameObject target;


	void Update() {
		CoinFollowTarget(target);
	}


	void OnTriggerEnter2D(Collider2D col) {
		if (col.tag == "Player" || col.tag == "Enemy") {
			ForCreatePoint.Instant.UpdateCoin();
			target = col.gameObject;
			gameObject.GetComponent<Animator>().SetBool("Collected", true);
		}
	}



	public void DeActiveCoinAfterCollect() {
		gameObject.SetActive(false);
		target = null;
	}


	void CoinFollowTarget(GameObject target) {
		if (target != null) {
			Vector3 vecLerp = Vector3.Lerp(transform.transform.position, target.transform.position, 0.4f);
			transform.position = vecLerp;
		}
	}





}
