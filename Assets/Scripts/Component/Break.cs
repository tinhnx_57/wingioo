﻿using UnityEngine;
using System.Collections;

public class Break : MonoBehaviour {

	private Animator anm;

	void Start () {
		Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
	}
}
