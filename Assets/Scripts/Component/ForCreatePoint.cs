﻿using UnityEngine;
using System.Collections;

public class ForCreatePoint : MonoBehaviour {

	public GameObject[] prefab;
	public int coinTotal ;
	public Player player;

	GameObject[] coinInGame;

	static ForCreatePoint createCmn;

	public static ForCreatePoint Instant {
		get { return createCmn;}
	}

	void Awake () {
		createCmn = this;
	}

	void Start () {
		coinInGame = new GameObject[coinTotal];
		StartCoroutine(InstantNewObj());
	}


	IEnumerator InstantNewObj() {
		CreateCoin();
		while (true) {
			yield  return new WaitForSeconds(Random.Range(0.5f, 1.5f));
			int randObj = Random.Range(0, prefab.Length);
			GameObject newObj = Instantiate(prefab[randObj], RandomPosForHealth(), Quaternion.identity) as GameObject;
		}
	}


	void CreateCoin() {
		for (int i=0; i <coinTotal; i++) {
			coinInGame[i] = ComponentControl.Instant.GetCoin();
			coinInGame[i].transform.position = RandomPosForCoin();
		}
	}


	public void UpdateCoin() {
		for (int i=0; i <coinTotal; i++) {
			if(!coinInGame[i].activeInHierarchy) {
				coinInGame[i].SetActive(true);
				coinInGame[i].transform.localScale = new Vector3(4, 4, 1);
				coinInGame[i].transform.position = RandomPosForCoin();
			}
		}
	}


	void ActiveCoin(GameObject coin) {
		coin.SetActive(true);
		coin.transform.position = RandomPosForCoin();
	}


	Vector2 RandomPosForHealth() {
		float randomX = Random.Range(-Background.Instant.getWidthBorder() + 0.5f, Background.Instant.getWidthBorder() - 0.5f);
		return new Vector2(randomX, Background.Instant.getHeightBorder());
	}


	Vector2 RandomPosForCoin() {
		float randomX = Random.Range(-Background.Instant.getWidthBorder() + 0.5f, Background.Instant.getWidthBorder() - 0.5f);
		float randomY = Random.Range(-Background.Instant.getHeightBorder()/2, Background.Instant.getHeightBorder());
		return new Vector2( randomX, randomY);
	}
}
