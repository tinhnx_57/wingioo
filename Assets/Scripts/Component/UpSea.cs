﻿using UnityEngine;
using System.Collections;

public class UpSea : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D (Collider2D col) {
		if (col.tag != "Player" && col.tag !="Enemy" && col.tag != "JoyStick" && col.tag != "Rada" && col.tag != "RadaLock" && col.tag != "RadaCatch") {
			Destroy(col.gameObject);
		}

		if (col.tag == "PBullet") {
			GameObject touchSea = ComponentControl.Instant.getTouchSea();
			touchSea.transform.position = col.transform.position;
		}

	}

}
