﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {


	public GameObject meshWater;
	public Material mat;
	public Background background;

	GameObject[] meshObject;
	Mesh[] mesh;

	float[] xPosition;
	float[] yPosition;
	float bottom;



	const float z = -1f;


	void Start () {
		SpawnWater(-background.boundOfBackgroundX(), 2 * background.boundOfBackgroundX(), transform.position.x, transform.position.x - 20f);
	}


	void SpawnWater(float left, float width, float top, float bot) {

		int edgeCount = Mathf.RoundToInt(width) * 2;
		int vertexCount = edgeCount + 1;

		meshObject = new GameObject[edgeCount];
		mesh = new Mesh[edgeCount];

		xPosition = new float[vertexCount];
		yPosition = new float[vertexCount];

		bottom = bot;

		for (int i = 0; i < vertexCount; i++) {
			xPosition[i] = left + (i * width)/ vertexCount;
			yPosition[i] = top;
		}

		for (int i = 0; i < edgeCount; i++) {
			mesh[i] = new Mesh();

			Vector3[] vertex = new Vector3[4];
			vertex[0] = new Vector3(xPosition[i], yPosition[i], z);
			vertex[1] = new Vector3(xPosition[i + 1], yPosition[i + 1], z);
			vertex[2] = new Vector3(xPosition[i], bottom, z);
			vertex[3] = new Vector3(xPosition[i + 1], bottom, z);

			Vector2[] UV = new Vector2[4];
			UV[0] = new Vector2(0, 0);
			UV[1] = new Vector2(1, 0);
			UV[2] = new Vector2(1, 1);
			UV[3] = new Vector2(0, 1);


			int[] tris = new int[6] {0, 1, 2, 2, 1, 3};

			mesh[i].vertices = vertex;
			mesh[i].uv = UV;
			mesh[i].triangles = tris;

			meshObject[i] = Instantiate(meshWater, transform.position + transform.up * 6, Quaternion.identity) as GameObject;
			meshObject[i].transform.parent = transform;
			meshObject[i].GetComponent<MeshFilter>().mesh = mesh[i];


		}
 
	}


	void UpdateMeshs() {
		for (int i = 0; i < mesh.Length; i++) {
			Vector3[] vertex = new Vector3[4];
			vertex[0] = new Vector3(xPosition[i], yPosition[i], z);
			vertex[1] = new Vector3(xPosition[i + 1], yPosition[i + 1], z);
			vertex[2] = new Vector3(xPosition[i], bottom, z);
			vertex[3] = new Vector3(xPosition[i + 1], bottom, z);

			mesh[i].vertices = vertex;
		}
	}



	void FixedUpdate() {
		for (int i = 0; i < yPosition.Length; i++) {
			yPosition[i] = 0.1f * Mathf.Sin(2*Mathf.PI * Time.time + i*0.2f);
		}
		UpdateMeshs();
	}


}
