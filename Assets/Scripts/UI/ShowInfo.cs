﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowInfo : MonoBehaviour { 
	public Player player;
	public Text yourScore;
	public Text[] txtName;
	public Text[] txtScore;
	public Text txtIndexOfPlayer;
	public Text txtKing;


	GameObject[] listEnemy;
	GameObject kingHat;
	static ShowInfo showInfo;
	int totalEnemy;

	void Awake() {
		showInfo = this;
	}


	public static ShowInfo Show{
		get { return showInfo;}
	} 
		

	void Update () {
		SortListEnemy();
		ShowLeaderBoardInfo();
		ShowKingName();
		KingHatFollowTheKing();
		ShowYourInfomation();
	}

	void ShowYourInfomation () {
		if ( yourScore.text != player.score.ToString()) {
			yourScore.text = player.score.ToString();
		}

		if (txtIndexOfPlayer.text != indexOfPlayerScoreWithListEnemy() + ".You") {
			txtIndexOfPlayer.text = indexOfPlayerScoreWithListEnemy() + ".You";
		}
	}


	public void GetListAndCreatKingHat() {
		totalEnemy = GenerateEnemy.Instant.GetTotalEnemy();
		listEnemy = new GameObject[totalEnemy];

		listEnemy = GenerateEnemy.Instant.GetListEnemy();
		kingHat = ComponentControl.Instant.GetKingHat();
		kingHat.transform.position = GetKingOfMap().transform.position;

	}


	void KingHatFollowTheKing() {
		kingHat.transform.position = new Vector3(GetKingOfMap().transform.position.x, GetKingOfMap().transform.position.y + 0.8f, 
			GetKingOfMap().transform.position.z) ;
	}


	 void SortListEnemy() {
		for(int i = 0; i < listEnemy.Length; i++)
			for(int j = i+1; j < listEnemy.Length; j++)
				if(listEnemy[i].GetComponent<Enemy>().score < listEnemy[j].GetComponent<Enemy>().score ) {
					GameObject temp = listEnemy[i];
					listEnemy[i] = listEnemy[j];
					listEnemy[j] = temp;
				}
	}


	int indexOfPlayerScoreWithListEnemy() {
		for(int i = 0; i < listEnemy.Length; i++) {
			if(listEnemy[i].GetComponent<Enemy>().score < player.score) {
				return i+1;
			}
		}
		return listEnemy.Length+1;
	}



	public GameObject GetKingOfMap() {
		int kingIndex = 0;
		if(indexOfPlayerScoreWithListEnemy() == 1 && !player.isDie) {
				return player.gameObject;
		}
		else {
			for(int i = 0; i < listEnemy.Length; i++) {
				if(listEnemy[i].GetComponent<Enemy>().state != Enemy.State.Break && !listEnemy[i].GetComponent<Enemy>().isDie) {
						kingIndex = i;
						break;
					}
				}
				return listEnemy[kingIndex].gameObject;
			}
			
	}



	void ShowLeaderBoardInfo() {
		for(int i = 0; i < txtName.Length; i++) {
			if(indexOfPlayerScoreWithListEnemy() - 1 != i)
				txtName[i].text = i+1 + "." + listEnemy[i].GetComponent<Enemy>().nameEnemy;
			else 
				txtName[i].text = i+1 + "." + player.namePlayer;
		}

		for(int i = 0; i < txtScore.Length; i++) {
			if(indexOfPlayerScoreWithListEnemy() - 1 != i)
				txtScore[i].text = listEnemy[i].GetComponent<Enemy>().score.ToString();
			else 
				txtScore[i].text = player.score.ToString();
				
		}

	}


	void ShowKingName() {
		if(listEnemy[0].GetComponent<Enemy>().score < player.score ) {
			if (txtKing.text != "King : " + player.namePlayer) {
				txtKing.text = "King : " + player.namePlayer;
			}
		}
		else if (txtKing.text != "King : " + listEnemy[0].GetComponent<Enemy>().nameEnemy) {
			txtKing.text = "King : " + listEnemy[0].GetComponent<Enemy>().nameEnemy;

		}
	}





}
