﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillSomeone : MonoBehaviour {
	public Text killInfo;
	float timeShowInfo;


	void Update () {
		if (timeShowInfo != 0 && Time.time - timeShowInfo >= 1) {
			gameObject.SetActive(false);
		}
	}

	public void SetKillInfo(string str) {
		killInfo.text = str;
	}

	public void SetColor(Color newcl) {
		killInfo.color = newcl;
	}

	public void SetTimeShowInfo () {
		timeShowInfo = Time.time;
	}

}
