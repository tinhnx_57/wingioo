﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
	public GameObject upPos;
	public GameObject rightPos;
	private float boundOfX;
	private float boundOfY;
	static Background background;


	void Awake () {
		background = this;
		boundOfX = transform.gameObject.GetComponent<SpriteRenderer>().bounds.size.x/2;
		boundOfY = transform.gameObject.GetComponent<SpriteRenderer>().bounds.size.y/2;

	}

	public static Background Instant {
		get {
			return background;
		}
	}



	public float getWidthBorder() {
		return rightPos.transform.position.x;
	}


	public float getHeightBorder() {
		return upPos.transform.position.y;
	}


	public float boundOfBackgroundX() {
		return boundOfX;
	}

	public float boundOfBackgroundY() {
		return boundOfY;
	}
}
