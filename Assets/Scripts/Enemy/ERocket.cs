﻿using UnityEngine;
using System.Collections;

public class ERocket : MonoBehaviour {
	public int index {get; private set;}
	public float speedRocket;
	Enemy ownEnemy;

	void Update () {
		transform.Translate(0,speedRocket,0);

		if(isOutOfScreen(gameObject))
			Destroy(gameObject);
			
	}


	public void SetOwnEnemy(Enemy own) {
		ownEnemy = own;
	}

	public Enemy GetOwnEnemy() {
		return ownEnemy;
	}

	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Player") {
			// solved in player trigger :D
		}
	}


	public bool isOutOfScreen(GameObject obj) {

		if(Mathf.Abs(obj.transform.position.x)> Background.Instant.boundOfBackgroundX() || 
			Mathf.Abs(obj.transform.position.y) > Background.Instant.boundOfBackgroundY())
			return true;
		return false;
	}



	public void SetERocketIndex(int id) {
		index = id;
	}

}

