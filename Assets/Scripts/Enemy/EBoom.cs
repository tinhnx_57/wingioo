﻿using UnityEngine;
using System.Collections;

public class EBoom : MonoBehaviour {
	public int index {get; private set;}
	Enemy ownEnemy;

	public void SetBoomIndex(int id) {
		index = id;
	}

	public void SetOwnEnemy(Enemy own) {
		ownEnemy = own;
	}

	public Enemy GetOwnEnemy() {
		return ownEnemy;
	}
}
