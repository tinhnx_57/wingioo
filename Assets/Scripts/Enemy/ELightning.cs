﻿using UnityEngine;
using System.Collections;

public class ELightning : MonoBehaviour {
	public int index {get; private set;}
	Enemy ownEnemy;


	public void SetELightningIndex(int id) {
		index = id;
	}

	public void SetOwnEnemy(Enemy own) {
		ownEnemy = own;
	}

	public Enemy GetOwnEnemy() {
		return ownEnemy;
	}


}
