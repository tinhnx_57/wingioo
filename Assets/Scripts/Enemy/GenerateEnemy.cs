﻿using UnityEngine;
using System.Collections;

public class GenerateEnemy : MonoBehaviour {
	public ListNameEnemy listname;

	public int enemyTotal;
	public float enemySpeed;
	static GenerateEnemy generateEnemy;

	GameObject[] listEnemy;


	void Awake() {
		generateEnemy = this;
	}

	public static GenerateEnemy Instant {
		get { return generateEnemy; }
	}


	public void Init() {
		listEnemy = new GameObject[enemyTotal];
		createEnemy();
	}


	public void UpdateEnemy() {
		for(int i=0; i< enemyTotal; i++) {
			if(!listEnemy[i].activeInHierarchy) {
				int angle = angleMovingOfEnemy();
				listEnemy[i].transform.position = randomPosition();
				listEnemy[i].transform.rotation = Quaternion.identity;
				listEnemy[i].transform.RotateAround(listEnemy[i].transform.position, Vector3.forward, angle);
				listEnemy[i].GetComponent<Enemy>().ReActiveComponentOfEnemy();
				listEnemy[i].SetActive(true);
			}
		}
	}


	void createEnemy() {
		for(int i=0; i< enemyTotal; i++) {
			int angle = angleMovingOfEnemy();
			listEnemy[i] = ComponentControl.Instant.GetEnemy();
			listEnemy[i].transform.position = randomPosition();
			listEnemy[i].transform.RotateAround(listEnemy[i].transform.position, Vector3.forward, angle);
			listEnemy[i].GetComponent<Enemy>().SetIndex(i);
			listEnemy[i].GetComponent<Enemy>().SetName(listname.GetName(i));
		}
	}


	public int GetTotalEnemy() {
		return enemyTotal;
	}


	public GameObject[] GetListEnemy() {
		return listEnemy;
	}

	public Vector3 randomPosition() {
		float randomX = Random.Range(-Background.Instant.getWidthBorder() + 0.5f, Background.Instant.getWidthBorder() - 0.5f);
		float randomY = Random.Range(-Background.Instant.getHeightBorder()/1.5f, Background.Instant.getHeightBorder() - 0.5f);
		return new Vector3(randomX, randomY, 0);
	}


	public int angleMovingOfEnemy()	{
		int randome = Random.Range(-17, 17);
		if(randome !=0) 
			return randome * 10;
		else return Random.Range(1, 17) * 10;
			
	}


}
