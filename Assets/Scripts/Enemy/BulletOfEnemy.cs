﻿using UnityEngine;
using System.Collections;

public class BulletOfEnemy : MonoBehaviour {


	public int index {get; private set;}
	public float movingSpeed;

	Vector2 startPostion;
	Enemy ownEnemy;
	Background background;
	float distanceToDestroy;

	void Start () {
		distanceToDestroy = 8.0f;
		startPostion = transform.position;
		background = GameObject.FindGameObjectWithTag("Background").GetComponent<Background>();
	}


	void Update () {
		movingBullet();
		destroyBulletOutOfScreen();
		DestroyBulletByDistance();
	}



	public void SetOwnEnemy(Enemy own) {
		ownEnemy = own;
	}

	public Enemy GetOwnEnemy() {
		return ownEnemy;
	}

	public void movingBullet() {
		transform.Translate(0, movingSpeed, 0);
	}


	void destroyBulletOutOfScreen() {
		if (Mathf.Abs(transform.position.x) > background.boundOfBackgroundX() || 
			Mathf.Abs(transform.position.y) > background.boundOfBackgroundY())
			Destroy(transform.gameObject);
	}

	void DestroyBulletByDistance () {
		if(Vector2.Distance(startPostion, transform.position) >= distanceToDestroy) {
			Destroy(gameObject);
		}
	}

	public void SetBulletIndex(int id) {
		index = id;
	}


	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Enemy" && col.GetComponent<Enemy>().index != index) {
//			Debug.Log(col.GetComponent<Enemy>().health);
		}
			

	}


}
