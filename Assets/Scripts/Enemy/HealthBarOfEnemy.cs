﻿using UnityEngine;
using System.Collections;

public class HealthBarOfEnemy : MonoBehaviour {

	Enemy ownEnemy;

	void Update () {
		HealthBarFollowOwnEnemy();

	}


	void HealthBarFollowOwnEnemy() {
		transform.position = new Vector3(ownEnemy.transform.position.x, ownEnemy.transform.position.y -0.7f, ownEnemy.transform.position.z);
	}



	public void SetOwnEnemy(Enemy enm) {
		ownEnemy = enm;
	}



}
