﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public enum State { Fly, Invulnerable, Falldown, Die, Break };
	public enum WeaponType { Normal, Boom, ThreeBullet, Rocket, Punch, Lightning };
	public GameObject eBullet;
	public GameObject head;
	public GameObject trailRenderer;
	public GameObject particleTail;
	public Transform target;


	public State	state {get; private set;}
	public WeaponType 	weapon {get; private set;} 
	public int 		angle {get; private set;}
	public int 		index {get; private set;}
	public float 	enemySpeed;
	public bool 	isDie {get; private set;}
	public string 	nameEnemy {get; private set;}
	public int 		score {get; private set;}
	public int 		health {get; private set;}

	GameObject eBulletInGame;
	GameObject punchInGame;
	GameObject lightningInGame;
	GameObject parentHealthBar;
	GameObject childHealthBar;
	GameObject borderHealth;
	Material EtailMaterial;

	Player player;
	Sprite[] thisSprite;

	const float HEALTHSTART = 100;
	static int countEnemyPursuit = 0;
	bool isUnvunerable;
	int 	totalBullet;
	float 	boundX, boundY;
	float 	timeForPunch, timeForLightning;
	float 	timeOutOfScreen;
	float borderHealthLengthWhenStart;
	float enemySpeedWhenStart;

	void Start () {
		health = 100;
		totalBullet = -10;
		isDie = false;
		isUnvunerable = true;
		enemySpeedWhenStart = enemySpeed;
		state = State.Fly;
		weapon = WeaponType.Normal;
		head.SetActive(false);
		particleTail.SetActive(false);
		trailRenderer.SetActive(false);
		thisSprite = new Sprite[14];
		EtailMaterial = new Material(Shader.Find("Unlit/Color"));
		EtailMaterial.SetColor("_Color",new Color(1,1,1));
		trailRenderer.GetComponent<TrailRenderer>().material = EtailMaterial;
		boundX = Background.Instant.getWidthBorder();
		boundY = Background.Instant.getHeightBorder();
		createHealthBar();
		borderHealthLengthWhenStart = borderHealth.GetComponent<SpriteRenderer>().bounds.size.x;
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		velocity = transform.up * maxSpeed * 0.5f;
		wanderTarget = new Vector3(0, wanderRadius, 0);

		CreateSpriteArray();
	}

	void OnEnable() {
		StartCoroutine(createComponent());
		StartCoroutine(EscapeUnvunerable());
	}



	void Update () {
		MovingEnemy();
		checkEnemyOutOfScreen();
		controlPunchAndLightning();
		ReturnNormalBullet();
		CheckEnemyState();
		ScaleBorderHealthBarWhenBeTheKingOrNot();
		TranslateChildHealthBar();
		LoadSpriteWhenRotate();
	}
		

	IEnumerator EscapeUnvunerable () {
		yield return new WaitForSeconds(3);
		isUnvunerable = false;
		trailRenderer.SetActive(true);
	}


	void LoadSpriteWhenRotate () {
		float eulerAnglesZ = transform.eulerAngles.z;
		Sprite spr = new Sprite();
		if (eulerAnglesZ >= 0 && eulerAnglesZ <= 15 ) {
			spr = thisSprite[0]; 
		}else if (eulerAnglesZ > 15 && eulerAnglesZ <= 30) {
			spr = thisSprite[1];
		}else if (eulerAnglesZ > 30 && eulerAnglesZ <= 45) {
			spr = thisSprite[2];
		}else if (eulerAnglesZ > 45 && eulerAnglesZ <= 55) {
			spr = thisSprite[3];
		}else if (eulerAnglesZ > 55 && eulerAnglesZ <= 70) {
			spr = thisSprite[4];
		}else if (eulerAnglesZ > 70 && eulerAnglesZ <= 80) {
			spr = thisSprite[5];
		}else if (eulerAnglesZ > 80 && eulerAnglesZ <= 90) {
			spr = thisSprite[6];
		}

		else if (eulerAnglesZ >= 90 && eulerAnglesZ <= 105 ) {
			spr = thisSprite[6];
		}else if (eulerAnglesZ > 105 && eulerAnglesZ <= 120) {
			spr = thisSprite[5];
		}else if (eulerAnglesZ > 120 && eulerAnglesZ <= 135) {
			spr = thisSprite[4];
		}else if (eulerAnglesZ > 135 && eulerAnglesZ <= 150) {
			spr = thisSprite[3];
		}else if (eulerAnglesZ > 150 && eulerAnglesZ <= 160) {
			spr = thisSprite[2];
		}else if (eulerAnglesZ > 160 && eulerAnglesZ <= 170) {
			spr = thisSprite[1];
		}else if (eulerAnglesZ > 170 && eulerAnglesZ <= 180) {
			spr = thisSprite[0];
		}

		else if (eulerAnglesZ > 180 && eulerAnglesZ <= 195 ) {
			spr = thisSprite[7];
		}else if (eulerAnglesZ > 195 && eulerAnglesZ <= 210) {
			spr = thisSprite[8];
		}else if (eulerAnglesZ > 210 && eulerAnglesZ <= 225) {
			spr = thisSprite[9];
		}else if (eulerAnglesZ > 225 && eulerAnglesZ <= 240) {
			spr = thisSprite[10];
		}else if (eulerAnglesZ > 240 && eulerAnglesZ <= 250) {
			spr = thisSprite[11];
		}else if (eulerAnglesZ > 250 && eulerAnglesZ <= 260) {
			spr = thisSprite[12];
		}else if (eulerAnglesZ > 260 && eulerAnglesZ <= 270) {
			spr = thisSprite[13];
		}

		else if (eulerAnglesZ > 270 && eulerAnglesZ <= 285 ) {
			spr = thisSprite[13];
		}else if (eulerAnglesZ > 285 && eulerAnglesZ <= 300) {
			spr = thisSprite[12];
		}else if (eulerAnglesZ > 300 && eulerAnglesZ <= 315) {
			spr = thisSprite[11];
		}else if (eulerAnglesZ > 315 && eulerAnglesZ <= 330) {
			spr = thisSprite[10];
		}else if (eulerAnglesZ > 330 && eulerAnglesZ <= 340) {
			spr = thisSprite[9];
		}else if (eulerAnglesZ > 340 && eulerAnglesZ <= 350) {
			spr = thisSprite[8];
		}else if (eulerAnglesZ > 350 && eulerAnglesZ <= 360) {
			spr = thisSprite[7];
		}

		gameObject.GetComponent<SpriteRenderer>().sprite = spr;
	}
		

	bool meetPlayerByDistance = false; // when player is the king
	void MovingEnemy () {
		if(timeOutOfScreen != 0 && Time.time - timeOutOfScreen < 1f) {
			MovingForward();
			checkOneTime = false;
		}else {
			if (player.IsTheKing() && Vector3.Distance(transform.position, player.transform.position) <= 10f && countEnemyPursuit <= 5) {
				meetPlayerByDistance = true;
				countEnemyPursuit ++; // have 6 enemy can pursuit player when he is the king
				UpdatePursuiting();
				RotateByVelocity();
				if (state == State.Break) {
					countEnemyPursuit --;
				}
			}
			else {
				if (player.IsTheKing() && meetPlayerByDistance) {
					countEnemyPursuit--;
					meetPlayerByDistance = false;
				}
				else if (!player.IsTheKing()) {
					countEnemyPursuit = 0;
				}
				RunOneTime();
				MovingWander();
			}

		}
	}
		

	bool checkOneTime;
	void RunOneTime () {
		if (!checkOneTime) {
			velocity = transform.up * maxSpeed * 0.5f;
			wanderTarget = new Vector3(0, wanderRadius, 0);
			checkOneTime = true;
		}
	}


	void CreateSpriteArray () {
		for (int i = 0; i < thisSprite.Length/2; i++) {
			thisSprite[i] = Resources.Load<Sprite>("SpritePlayer/RotateLeft/Player" + (i + 1));
		}

		for (int i = thisSprite.Length/2; i < thisSprite.Length; i++) {
			thisSprite[i] = Resources.Load<Sprite>("SpritePlayer/RotateRight/Player" + (i - 6));
		}
	}



	void TranslateChildHealthBar() {
		if(IsTheKing()) {
			ScaleChildHealthBar(2);
		}
		else { ScaleChildHealthBar(1); }

	}


	void ScaleBorderHealthBarWhenBeTheKingOrNot () {
		if (IsTheKing() && Mathf.Approximately(borderHealth.GetComponent<SpriteRenderer>().bounds.size.x, borderHealthLengthWhenStart)) {
			ScaleHealthBarWhenBeTheKing();
			ScaleThisEnemyWhenBeTheKing();
			ScaleChildHealthBar(2);
		}
		else if (!IsTheKing() && Mathf.Approximately(borderHealth.GetComponent<SpriteRenderer>().bounds.size.x, borderHealthLengthWhenStart * 2)) {
			ReturnPlayerFromKing();
			ScaleThisEnemyNormal();
			ScaleChildHealthBar(1);
		}
	}


	public void ScaleHealthBarWhenBeTheKing() {
		health *= 2;
		borderHealth.transform.localScale = new Vector3(2,1,0);
		childHealthBar.transform.localScale = new Vector3(2,1,0);

	}

	public bool IsTheKing() {
		if(ShowInfo.Show.GetKingOfMap() == gameObject)
			return true;
		return false;
	}

	public void ReturnPlayerFromKing() {
		health /= 2;
		borderHealth.transform.localScale = new Vector3(1,1,0);
		childHealthBar.transform.localScale = new Vector3(1,1,0);
	}

	void ScaleThisEnemyWhenBeTheKing() {
		transform.localScale = new Vector3(2.2f, 2.2f, 2.2f);
	}

	void ScaleThisEnemyNormal() {
		transform.localScale = new Vector3(2, 2, 2);
	}

	void ScaleChildHealthBar(int index) {
		if(health >= 0 && health <= HEALTHSTART * index && state != State.Break) {
			childHealthBar.transform.localScale =  new Vector3(health/HEALTHSTART, 1, 1);
			Vector3 locVec = new Vector3(parentHealthBar.transform.position.x -((HEALTHSTART * index - health)/2 
				* (borderHealthLengthWhenStart/(HEALTHSTART * index))) 
				* index, parentHealthBar.transform.position.y, parentHealthBar.transform.position.z);

			childHealthBar.transform.position = locVec;
			if(health >= 60 * index)
				childHealthBar.GetComponent < SpriteRenderer>().color = new Color(29.0f/255, 148.0f/255,40.0f/255);
			else if(health > 30 * index && health < 60 * index )
				childHealthBar.GetComponent<SpriteRenderer>().color = new Color(234.0f/255, 112.0f/255,112.0f/255);
			else if(health <= 30 * index)
				childHealthBar.GetComponent<SpriteRenderer>().color = new Color(1, 0,0);
		}
	}




	public void ReturnNormalBullet() {
		if(totalBullet == 0)	{
			ChangeWeapon(WeaponType.Normal);
			Color normalColor = new Color(255, 255, 255);
			ChangeTailColor(normalColor);
		}
	}
		

	IEnumerator SpeedUpWhenHaveCoin() {
		head.SetActive(true);
		yield return new WaitForSeconds(1);
		head.SetActive(false);
	}


	void ChangeWeapon(WeaponType ChangeTo) {
		weapon = ChangeTo;
	}

	public void ChangeTailColor(Color col) {
		EtailMaterial.SetColor("_Color", col);
	}

	public void DecreaseBullet(int a) {
		totalBullet -= a;
		if(totalBullet <0)
			totalBullet =0;
	}


	void ChangeTotalBullet(int tol)	{
		totalBullet = tol;
	}

	public void checkEnemyOutOfScreen() {
		float angleEnemy = transform.eulerAngles.z;
		if(isOutOfUpBorder()) {
			timeOutOfScreen = Time.time;
			if(angleEnemy >= 0 && angleEnemy < 90) {
				transform.rotation = Quaternion.Euler(0, 0, 180 - angleEnemy);
			}
			else if(angleEnemy >= 270 && angleEnemy < 360)
				transform.rotation = Quaternion.Euler(0, 0, 540 - angleEnemy);
		}

		else if(isOutOfBottomBorder()) {
			if (state == State.Fly) {
				timeOutOfScreen = Time.time;
				if (angleEnemy >= 180 && angleEnemy < 270) {
					transform.rotation = Quaternion.Euler(0, 0, 540 - angleEnemy);
				}
				else if (angleEnemy >= 90 && angleEnemy < 180)
					transform.rotation = Quaternion.Euler(0, 0, 180 - angleEnemy);
			}
			else if (state == State.Falldown) {
				particleTail.SetActive(false);
				Break();
			}

			
		}else if (isOutOfLeftBorder()) {
			timeOutOfScreen = Time.time;
			if (angleEnemy >= 0 && angleEnemy < 90) {
				transform.rotation = Quaternion.Euler(0, 0, 360 - angleEnemy);
			}
			else if(angleEnemy >= 90 && angleEnemy < 180)
				transform.rotation = Quaternion.Euler(0, 0, 360 - angleEnemy);
			
		}else if(isOutOfRightBorder()) {
			timeOutOfScreen = Time.time;
			if (angleEnemy >= 180 && angleEnemy < 270) {
				transform.rotation = Quaternion.Euler(0, 0, 360 - angleEnemy);
			}
			else if(angleEnemy >= 270 && angleEnemy < 360) 
				transform.rotation = Quaternion.Euler(0, 0, 360 - angleEnemy);
		}

	}


	public bool isOutOfUpBorder() {
		if (transform.position.y > boundY)
			return true;
		return false;
	}


	public bool isOutOfBottomBorder() { 
		if (transform.position.y < -boundY) 
			return true;
		return false;
	}


	public bool isOutOfRightBorder() {
		if (transform.position.x > boundX)
			return true;
		return false;
	}

	public bool isOutOfLeftBorder() {
		if (transform.position.x < -boundX)
			return true;
		return false;
	}


	void createHealthBar() {
		parentHealthBar = ComponentControl.Instant.getHealthBar();
		parentHealthBar.GetComponent<HealthBarOfEnemy>().SetOwnEnemy(this);
		parentHealthBar.transform.position = new Vector3(transform.position.x, transform.position.y -0.7f, transform.position.z);
		borderHealth = parentHealthBar.transform.GetChild(0).gameObject;
		childHealthBar = parentHealthBar.transform.GetChild(1).gameObject;
	}


	void controlPunchAndLightning()	{
		if(punchInGame != null)	{
			punchInGame.transform.position = transform.position;
			if(Time.time - timeForPunch > 0.3f || state == State.Break)
				Destroy(punchInGame);
		}	
		if(lightningInGame != null)	{
			lightningInGame.transform.position = transform.position;
			if(Time.time - timeForLightning > 0.3f || state == State.Break)
				Destroy(lightningInGame);
		}			
	}

	void MovingForward()	{
		if (state != State.Falldown) {
			transform.Translate(0,enemySpeed,0);
		}
	}






	#region WanderMoving

	protected Vector3 velocity;

	Vector3 force;
	public float mass = 1f;
	public float maxSpeed = 5f;

	public float wanderRadius = 1f;
	public float wanderDistance = 1f;
	public float wanderJitter = 0.2f;
	protected Vector3 wanderTarget;

	[Header("Seek Params")]
	public float seekModifier = 1f;

	void UpdateWandering () {
		force = Wander();
		Integrate(force, Time.deltaTime);


	}


	void MovingWander () {
		if (state != State.Falldown) {
			UpdateWandering();
			if (velocity.sqrMagnitude > 0.00001) {
				float alpha = -90 + Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
				Quaternion rot = Quaternion.Euler(new Vector3(0, 0, alpha));
				transform.rotation = rot;
			}
		}
	}

	// Integrate Newton's law of motion
	protected void Integrate (Vector3 force, float dt){
		Vector3 acceleration = force / mass;
		// Update velocity
		velocity += acceleration * dt;

		// Clamp velocity
		velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

		// Update position
		transform.position += velocity * dt * 1.5f;

	}


	protected Vector3 Wander () {
		wanderTarget += new Vector3(BinomialRandom() * wanderJitter, BinomialRandom() * wanderJitter, 0);
		wanderTarget.Normalize();
		wanderTarget *= wanderRadius;

		Vector3 targetLocal = wanderTarget + new Vector3(0, wanderDistance, 0);
		Vector3 target = transform.TransformPoint(targetLocal);
		 
		return target - transform.position;
	}


	public static float BinomialRandom () {
		return Random.value - Random.value;
	}


	void UpdatePursuiting () {
		Player evader = player.gameObject.GetComponent<Player>();
		force = Persuit(evader);
		Integrate(force, Time.deltaTime);
	}

	protected Vector3 Seek (Vector3 target) {
		Vector3 desiredVelocity = (target - transform.position).normalized * maxSpeed;
		return (desiredVelocity - velocity) * seekModifier;
	}

	protected Vector3 Persuit (Player evader) {
		Vector3 toEvader = evader.transform.position - transform.position;
		float relativeHeading = Vector3.Dot (transform.forward, evader.transform.forward);

		if (Vector3.Dot (transform.forward, toEvader) > 0 && relativeHeading < -0.95) {
			return Seek(evader.transform.position);
		}

		float lookAheadTime = toEvader.magnitude / (maxSpeed + evader.Velocity.magnitude);
		return Seek(evader.transform.position + evader.Velocity * lookAheadTime);

	}


	void RotateByVelocity() {
		if (velocity.sqrMagnitude > 0.00001) {
			float alpha = -90 + Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
			Quaternion rot = Quaternion.Euler(new Vector3(0, 0, alpha));
			transform.rotation = rot;

		}
	}

	#endregion





	public void setAngle(int agl) {
		angle = agl;
	}


	IEnumerator createComponent() {
		yield return new WaitForSeconds(0.5f);
		while(true) {
			Vector3 pos = new Vector3(transform.position.x, transform.position.y, transform.position.z+2);
			switch (weapon) {
				case WeaponType.Normal:
					GameObject bul = ComponentControl.Instant.getEBullet() as GameObject;
					bul.GetComponent<BulletOfEnemy>().SetBulletIndex(index);
					bul.GetComponent<BulletOfEnemy>().SetOwnEnemy(this);
					bul.transform.rotation = transform.rotation;
					bul.transform.position = pos + transform.up;
					yield return new WaitForSeconds(0.7f);
					break;

				case WeaponType.Boom:

					GameObject boom = ComponentControl.Instant.getEBoom() as GameObject;
					boom.GetComponent<EBoom>().SetBoomIndex(index);
					boom.GetComponent<EBoom>().SetOwnEnemy(this);
					boom.transform.position = pos;
					DecreaseBullet(1);
					yield return new WaitForSeconds(1f);
					break;

				case WeaponType.ThreeBullet:
					GameObject[] _3bullet = new GameObject[3];
					for(int i = 0; i < _3bullet.Length; i++){
						_3bullet[i] = ComponentControl.Instant.getEBullet();
						_3bullet[i].transform.position = pos;
						_3bullet[i].transform.rotation = transform.rotation;
						_3bullet[i].GetComponent<BulletOfEnemy>().SetBulletIndex(index);
						_3bullet[i].GetComponent<BulletOfEnemy>().SetOwnEnemy(this);

					}
					_3bullet[0].transform.RotateAround(_3bullet[0].transform.position, Vector3.forward, 3.0f);
					_3bullet[2].transform.RotateAround(_3bullet[2].transform.position, Vector3.forward, -3.0f);

					DecreaseBullet(3);
					yield return new WaitForSeconds(0.6f);
					break;

				case WeaponType.Rocket:
					GameObject rocket = ComponentControl.Instant.getERocket() as GameObject;
					rocket.GetComponent<ERocket>().SetERocketIndex(index);
					rocket.GetComponent<ERocket>().SetOwnEnemy(this);
					rocket.transform.position = pos;
					rocket.transform.rotation = transform.rotation;
					DecreaseBullet(1);
					yield return new WaitForSeconds(1.6f);
					break;

				case WeaponType.Punch:
					if(punchInGame == null)
					{
						punchInGame = ComponentControl.Instant.getEPunch() as GameObject;
						punchInGame.GetComponent<EPunch>().SetEPunchIndex(index);
						punchInGame.GetComponent<EPunch>().SetOwnEnemy(this);
						punchInGame.transform.position = pos;
						punchInGame.transform.rotation = transform.rotation;
						timeForPunch = Time.time;
					}else
						Destroy(punchInGame);
					DecreaseBullet(1);
					yield return new WaitForSeconds(1f);
					break;

				case WeaponType.Lightning:
					if(lightningInGame == null) {
						lightningInGame = ComponentControl.Instant.getELightning() as GameObject;
						lightningInGame.GetComponent<ELightning>().SetELightningIndex(index);
						lightningInGame.GetComponent<ELightning>().SetOwnEnemy(this);
						lightningInGame.transform.position = pos;
						lightningInGame.transform.rotation = transform.rotation;
						timeForLightning = Time.time;
					}else
						Destroy(lightningInGame);

					DecreaseBullet(1);
					yield return new WaitForSeconds(1f);
					break;
			}

		}
	}

	void CollisionWithEnemy () {
		StartCoroutine(ShakePlayer());
	}

	IEnumerator ShakePlayer() {
		float rotZ = transform.eulerAngles.z;
		transform.rotation = Quaternion.Euler(0, 0, rotZ - 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ + 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ - 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ + 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ - 10);
		yield return new WaitForSeconds (0.05f);
		transform.rotation = Quaternion.Euler(0, 0, rotZ);

	}


	void OnTriggerEnter2D(Collider2D col) {
		string tag = col.tag;
		Color tailColor = new Color();

		if(state != State.Break) {
			if(tag == "3Bullet" || tag == "Boom" || tag == "Lightning" || tag == "Punch" || tag == "Rocket" || tag == "Health") {
				score += 25;
			}

			if (punchInGame == null && !isUnvunerable) {
				switch (tag) {
					case "Player":
						if (player.havePunch == false) {
							health = health - 90;
							if (health <= 0) {
								health = 0;
								player.PlayerKillEnemy(gameObject);
							}
						}
						break;

					case "PPunch":
						health -= 100;
						if (health <= 0) {
							health = 0;
							player.PlayerKillEnemy(gameObject);
						}
						break;

					case "PBullet":
						Destroy(col.gameObject);
						health = health - 20;
						if(health <= 0) {
							health = 0;
							player.PlayerKillEnemy(gameObject);
						}
						break;

					case "PRocket":
						health -= 100;
						if(health <= 0) {
							health = 0;
							player.PlayerKillEnemy(gameObject);
						}
						break;

					case "PBoom":
						health -= 100;
						if (health <= 0) {
							health = 0;
							player.PlayerKillEnemy(gameObject);
						}
						Destroy(col.gameObject);
						break;

					case "PLightning":
						health -= 80;
						if (health <= 0) {
							health = 0;
							player.PlayerKillEnemy(gameObject);
						}
						break;

					case "Enemy":
						health -= 80;
						if (health <= 0) {
							health = 0;
							col.GetComponent<Enemy>().KillSomeOne();
						}
						break;

					case "EBullet":
						if(col.GetComponent<BulletOfEnemy>().index != index) {
							health -= 40;
							if(health <= 0) {
								health = 0;
								col.GetComponent<BulletOfEnemy>().GetOwnEnemy().KillSomeOne();
							}
							Destroy(col.gameObject);
						}
						break;

					case "EBoom":
						if(col.GetComponent<EBoom>().index != index) {
							health -= 100;
							if(health <= 0) {
								health = 0;
								col.GetComponent<EBoom>().GetOwnEnemy().KillSomeOne();
							}
							Destroy(col.gameObject);
						}
						break;


					case "ERocket":
						if(col.GetComponent<ERocket>().index != index) {
							health -= 100;
							if(health <= 0) {
								health = 0;
								col.GetComponent<ERocket>().GetOwnEnemy().KillSomeOne();
							}
							Destroy(col.gameObject);
						}
						break;

					case "EPunch":
						if(col.GetComponent<EPunch>().index != index) {
							health -= 100;
							if(health <= 0) {
								health = 0;
								col.GetComponent<EPunch>().GetOwnEnemy().KillSomeOne();
							}
						}
						break;

					case "ELightning":
						if(col.GetComponent<ELightning>().index != index) {
							health -= 80;
							if(health <= 0) {
								health = 0;
								col.GetComponent<ELightning>().GetOwnEnemy().KillSomeOne();
							}
						}
						break;
				}

			}


				
			switch (tag) {
				case "Coin":
					score = score + 5;
					StartCoroutine(SpeedUpWhenHaveCoin());
					break;

				case "Health":
					if(IsTheKing()) {
						health += 100;
					}
					else { 
						health = 100; 
					}
					col.gameObject.SetActive (false);
					break;
				case "UpSea":
					if(state == State.Falldown) {
						particleTail.SetActive(false);
						Break();
					}
					break;


				case "Boom":
					changeWeapon(WeaponType.Boom);
					ChangeTotalBullet(10);
					tailColor = new Color(0.32f,0.09f,0.09f);
					changeMaterialColor(tailColor);
					col.gameObject.SetActive (false);
					break;

				case "3Bullet":
					changeWeapon(WeaponType.ThreeBullet);
					ChangeTotalBullet(300);
					tailColor = new Color(0.94f,0.45f,0.19f);
					changeMaterialColor(tailColor);
					col.gameObject.SetActive (false);
					break;

				case "Rocket":
					changeWeapon(WeaponType.Rocket);
					ChangeTotalBullet(10);
					tailColor = new Color(0.9f,0,0);
					changeMaterialColor(tailColor);
					col.gameObject.SetActive (false);
					break;

				case "Punch":
					changeWeapon(WeaponType.Punch);
					ChangeTotalBullet(8);
					tailColor = new Color(0.8f,0.08f,0.92f);
					changeMaterialColor(tailColor);
					col.gameObject.SetActive (false);
					break;

				case "Lightning":
					changeWeapon(WeaponType.Lightning);
					ChangeTotalBullet(8);
					tailColor = new Color(0.2f,0.35f,0.86f);
					changeMaterialColor(tailColor);
					col.gameObject.SetActive (false);
					break;
			}
		}

	}
		

	public void CheckEnemyState() {
		if((!IsTheKing() && health <= 10 && health > 0) || (IsTheKing() && health <= 20 && health > 0)) {
			if(state != State.Falldown) {
				FallDown();
				if (weapon == WeaponType.Punch) {
					ChangeWeapon(WeaponType.Normal);
					Color normalColor = new Color(255, 255, 255);
					ChangeTailColor(normalColor);
				}
			}
				
		}
		else if(health == 0) {
				particleTail.SetActive(false);
				Create3PartAfterBreak();
				CreateComponentAfterDie();
				CreateBreakComponent();
				Break();
		}
		else {
			if(state == State.Falldown) {
				transform.GetComponent<Rigidbody2D> ().gravityScale = 0;
				state = State.Fly;
				particleTail.SetActive(false);
				SetSpeedMoving(enemySpeedWhenStart);
			}

		}

	}


	bool create3PartOneTime = false;
	void Create3PartAfterBreak() {
		if(!create3PartOneTime) {
			GameObject[] threePart = new GameObject[3];
			for(int i = 0; i < 3; i++) {
				threePart[i] = ComponentControl.Instant.GetOneOf3PartAfterBreak();
				threePart[i].transform.position = transform.position;
				AddForceForOneOf3Part(threePart[i]);

			}

			create3PartOneTime = true;
		}

	}


	void AddForceForOneOf3Part(GameObject target) {
		target.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f,1f), Random.Range(-1f,1f)).normalized * 150);
	}


	void CreateComponentAfterDie () {
		switch (weapon) {
			case WeaponType.Boom:
				GameObject boom = ComponentControl.Instant.GetBoomComponent();
				boom.transform.position = transform.position;
				break;


		case WeaponType.ThreeBullet:
			GameObject threeBullet = ComponentControl.Instant.GetThreeBulletComponent();
			threeBullet.transform.position = transform.position;
			break;

		case WeaponType.Rocket:
			GameObject rocket = ComponentControl.Instant.GetRocketComponent();
			rocket.transform.position = transform.position;
			break;

		case WeaponType.Punch:
			GameObject punch = ComponentControl.Instant.GetPunchComponent();
			punch.transform.position = transform.position;
			break;

		case WeaponType.Lightning:
			GameObject lightning = ComponentControl.Instant.GetLightningComponent();
			lightning.transform.position = transform.position;
			break;
		}
	}

	void CreateBreakComponent () {
		if (!isOutOfBottomBorder() && !isOutOfUpBorder() && !isOutOfLeftBorder() && !isOutOfRightBorder()) {
			GameObject breakComponent = ComponentControl.Instant.GetBreakComponent();
			breakComponent.transform.position = transform.position;
		}

	}


	void KillSomeOne() {
		if(state == State.Falldown) {
			GetComponent<Rigidbody2D>().AddForce(transform.up);
		}

		score += 20;
		if(IsTheKing()) {
			if(health + 40  <= 200)
				health += 40;
			else
				health = 200;
		}
		else {
			if(health + 20  <= 100)
				health += 20;
			else
				health = 100;
		}
			
	}
		

	public void changeWeapon(WeaponType wp)	{
		weapon = wp;
	}
		

	void FallDown() {
		state = State.Falldown;
		particleTail.SetActive(true);
		gameObject.GetComponent<Rigidbody2D>().gravityScale = 1f;
		SetSpeedMoving(0);
	}


	void Break() {
		GenerateEnemy.Instant.UpdateEnemy();
		state = State.Break;
		head.SetActive(false);
		trailRenderer.SetActive(false);
		gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
		if (parentHealthBar != null) parentHealthBar.SetActive(false);
		gameObject.SetActive(false);
	}


	void SetSpeedMoving(float speed) {
		enemySpeed = speed;
	}


		

	public void ReActiveComponentOfEnemy() {
			isDie = false;
			health = 100;
			score = score/2;
			create3PartOneTime = false;
			trailRenderer.SetActive(false);
			isUnvunerable = true;
			parentHealthBar.transform.position = new Vector3(transform.position.x, transform.position.y -0.7f, transform.position.z);
			parentHealthBar.SetActive(true);
			state = State.Fly;
			weapon = WeaponType.Normal;
			childHealthBar.transform.localScale =  new Vector3(1,1,1);
			childHealthBar.GetComponent<SpriteRenderer>().color = new Color(29.0f/255, 148.0f/255,40.0f/255);
			changeMaterialColor(new Color(1,1,1));
			SetSpeedMoving(enemySpeedWhenStart);
			gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
			particleTail.SetActive(false);
			
	}



	void changeMaterialColor(Color color) {
		EtailMaterial.SetColor("_Color",color);
	}



	public void SetIndex(int id) {
		index = id;
	}


	public void SetName(string str) {
		nameEnemy = str;
	}



}
