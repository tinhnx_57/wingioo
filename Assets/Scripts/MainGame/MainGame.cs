﻿using UnityEngine;
using System.Collections;

public class MainGame : MonoBehaviour {
	public Player player;
	public Canvas mainCanvas;
	public Canvas gameOverCanvas;
	public Canvas pauseCanvas;
	public static float TIME_LOAD_NEW_GAME = 0.8f;
	static MainGame mainGame;

	public static MainGame Instant {
		get {
			return mainGame;
		}
	}

	void Awake() {
		Application.targetFrameRate = 60;
		mainGame = this;
	}


	void Start () {
		GenerateEnemy.Instant.Init();
		ShowInfo.Show.GetListAndCreatKingHat();

	}



	void Update () {
		ControlBackButton();
	}

	void ControlBackButton () {
		if (Input.GetKey(KeyCode.Escape)) {
			PauseGame();
		}		
	}


	void PauseGame () {
		mainCanvas.gameObject.SetActive(false);
		pauseCanvas.gameObject.SetActive(true);
	}
		

	public void LoadCanvasWhenDie() {
		StartCoroutine(WaitDie());
	}


	IEnumerator WaitDie() {
		yield return new WaitForSeconds(player.timeLoadNewGame);
		mainCanvas.gameObject.SetActive(false);
		pauseCanvas.gameObject.SetActive(false);
		gameOverCanvas.gameObject.SetActive(true);


	}


	public void PlayAgain() {
		gameOverCanvas.gameObject.SetActive(false);
		mainCanvas.gameObject.SetActive(true);
		pauseCanvas.gameObject.SetActive(false);
		Vector3 againPos = new Vector3(Random.Range(-Background.Instant.getWidthBorder()/1.2f, Background.Instant.getWidthBorder()/1.2f),
			Random.Range(-Background.Instant.getHeightBorder()/2f, Background.Instant.getHeightBorder()/2f), 0);
		
		player.transform.position = againPos;
		player.PlayAgain();

	}



	public void exitGame() {
		Application.Quit();
	}


	public static float GetTimeLoadGame () {
		return TIME_LOAD_NEW_GAME;
	}


}
