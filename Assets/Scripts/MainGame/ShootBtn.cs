﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShootBtn : MonoBehaviour {
	public Player player;

	GameObject punchInGame;
	GameObject lightInGame;

	bool canPunch;
	bool canLight;
	bool holdShootBtn;
	float timeStart;
	float timeForPunch;
	float timeForLight;


	void Start () {
		timeStart =Time.time;
		canPunch = true;
	}
	

	public void PointerDown() {
		holdShootBtn = true;
	}

	public void PointerUp()	{
		holdShootBtn = false;
	}



	void Update() {
		if(player.state == Player.PlayerState.Break || !player.gameObject.activeInHierarchy) {
			holdShootBtn = false;
		}


		if(holdShootBtn && player.state != Player.PlayerState.Break && player.gameObject.activeInHierarchy && !player.IsUnvunerable()) {
			shoot();
		}
			

		if((player.weapon != Player.WeaponType.Punch && punchInGame != null) || player.state == Player.PlayerState.Break) {
			Destroy(punchInGame);
		}
			

		if(punchInGame != null) {
			if(player.isUnderSea) {
				Destroy(punchInGame);
				player.ChangeHavePunch(false);
			}

			if(Time.time - timeForPunch < 0.6f) {
				player.ChangeRotation(false);
			}
				
			else  if(Time.time - timeForPunch > 0.6f) {
				Destroy(punchInGame);
				player.ChangeHavePunch(false);


			}
			canPunch = false;
		}
		else {
			player.ChangeHavePunch(false);
			if(Time.time - timeForPunch > 1f && !player.isUnderSea)
				canPunch = true;
			
			if(player.underTheSea()) {
				player.ChangeRotation(false);
			}else {
				if(player.isEscapeSea())
					player.ChangeRotation(true);
			}
				

		}


		if((player.weapon != Player.WeaponType.Lightning && lightInGame != null) || player.state == Player.PlayerState.Break)
			Destroy(lightInGame);


		if(lightInGame != null && Time.time - timeForLight > 0.1f) {
			Destroy(lightInGame);
			canLight = false;
		}
		else if(lightInGame == null && Time.time - timeForLight >1f) {
			canLight = true;
		}
			
	}


	public void shoot()	{ 
		if (!player.IsUnvunerable() && player.isActiveAndEnabled) {
			Player.WeaponType weapon = player.weapon;
			Vector3 vec = new Vector3(player.transform.position.x,player.transform.position.y,player.transform.position.z + 2);

			switch(weapon) {
			case Player.WeaponType.Normal:
				if(Time.time > timeStart) {
					GameObject bul = ComponentControl.Instant.GetBullet();
					bul.transform.rotation = player.transform.rotation;
					bul.transform.position = vec + player.transform.up * 0.85f;
					timeStart = Time.time + 0.15f;

				}
				break;

			case Player.WeaponType.Boom:
				if(Time.time > timeStart) {
					GameObject boom = ComponentControl.Instant.GetBoom();
					boom.transform.position =  vec;
					timeStart = Time.time + 0.8f;
					player.DecreaseBullet(1);
				}
				break;

			case Player.WeaponType.ThreeBullet:
				if(Time.time > timeStart) {
					GameObject[] bullet = new GameObject[3];
					for(int i = 0; i < bullet.Length; i++) {
						bullet[i] = ComponentControl.Instant.GetBullet();
						bullet[i].transform.rotation = player.transform.rotation;
						bullet[i].transform.position = vec + player.transform.up * 0.85f;


					}
					bullet[0].transform.RotateAround(bullet[0].transform.position, Vector3.forward, 3.0f);
					bullet[2].transform.RotateAround(bullet[2].transform.position, Vector3.forward, -3.0f);
					timeStart = Time.time + 0.3f;
					player.DecreaseBullet(3);
				}
				break;

			case Player.WeaponType.Rocket:
				if(Time.time > timeStart) {
					GameObject rocket = ComponentControl.Instant.GetRocket();
					rocket.transform.position = vec;
					rocket.transform.rotation = player.transform.rotation;
					timeStart = Time.time + 1.5f;
					player.DecreaseBullet(1);
				}
				break;

			case Player.WeaponType.Punch:
				if(punchInGame == null && canPunch) {
					punchInGame = ComponentControl.Instant.GetPunch();
					punchInGame.transform.position = vec;
					punchInGame.transform.rotation = player.transform.rotation;
					timeForPunch = Time.time;
					player.SpeedUpByPunch();
					player.ChangeRotation(false);
					player.DecreaseBullet(1);
					player.ChangeHavePunch(true);
				}
				break;

			case Player.WeaponType.Lightning: 
				if(lightInGame == null && canLight) {
					lightInGame = ComponentControl.Instant.GetLightning();
					lightInGame.transform.position = vec;
					lightInGame.transform.rotation = player.transform.rotation;
					timeForLight = Time.time;
					player.DecreaseBullet(1);
				}
				break;
			}

			player.ShowWeaponLeft();
		}




	}
		

}
