﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public Canvas mainCanvas;
	public Canvas pauseCanvas;


	public void ContinueGame () {
		mainCanvas.gameObject.SetActive(true);
		pauseCanvas.gameObject.SetActive(false);
	}
		

	public void QuitGame () {
		Application.Quit();
	}
}
