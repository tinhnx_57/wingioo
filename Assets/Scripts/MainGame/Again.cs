﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Again : MonoBehaviour {
	public Text yourScore;
	public Text bestScore;


	void OnEnable() {
		bestScore.text = "BEST: " + PlayerPrefs.GetInt("BEST").ToString();
		yourScore.text = PlayerPrefs.GetInt("YOURSCORE").ToString();
	}
		

	public void exitGame() {
		Application.Quit();
	}
}
