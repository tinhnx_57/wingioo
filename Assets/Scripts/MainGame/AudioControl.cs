﻿using UnityEngine;
using System.Collections;

public class AudioControl : MonoBehaviour {

	public GameObject player;

	static AudioControl audioControl;

	void Awake () {
		audioControl = this;
	}

	public static AudioControl Instant {
		get {
			return audioControl;
		}
	}


	public void PlayerDie () {
		AudioSource playerDie = player.GetComponent<AudioSource>();
		playerDie.Play();
	}



}
